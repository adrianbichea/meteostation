#ifndef FONTPREFERENCES_H
#define FONTPREFERENCES_H

#include <QString>
#include <QFont>

class FontPreferences
{
public:
    FontPreferences();
    FontPreferences(QString fontName, bool bold, bool italic, bool underline, int size);
    ~FontPreferences();


    QString getFontName() const;
    void setFontName(const QString &value);

    bool getBold() const;
    void setBold(bool value);

    bool getItalic() const;
    void setItalic(bool value);

    bool getUnderline() const;
    void setUnderline(bool value);

    int getSize() const;
    void setSize(int value);

    QString toString();
    void fromString(QString);

    QFont getFont();
    QString getStyleSheet();

private:
    QString fontName;
    bool bold;
    bool italic;
    bool underline;
    int size;
};

#endif // FONTPREFERENCES_H
