#ifndef FORECASTSYS_H
#define FORECASTSYS_H

#include <QString>

class ForecastSys
{
public:
    ForecastSys();

    QString getPod() const;
    void setPod(const QString &value);

private:
    QString pod;
};

#endif // FORECASTSYS_H
