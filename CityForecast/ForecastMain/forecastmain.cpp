#include "forecastmain.h"

ForecastMain::ForecastMain()
{

}

double ForecastMain::getTemp() const
{
    return temp;
}

void ForecastMain::setTemp(double value)
{
    temp = value;
}

double ForecastMain::getFeels_like() const
{
    return feels_like;
}

void ForecastMain::setFeels_like(double value)
{
    feels_like = value;
}

double ForecastMain::getTemp_min() const
{
    return temp_min;
}

void ForecastMain::setTemp_min(double value)
{
    temp_min = value;
}

double ForecastMain::getTemp_max() const
{
    return temp_max;
}

void ForecastMain::setTemp_max(double value)
{
    temp_max = value;
}

double ForecastMain::getPressure() const
{
    return pressure;
}

double ForecastMain::getSea_level() const
{
    return sea_level;
}

double ForecastMain::getGrnd_level() const
{
    return grnd_level;
}

void ForecastMain::setGrnd_level(double value)
{
    grnd_level = value;
}

double ForecastMain::getHumidity() const
{
    return humidity;
}

void ForecastMain::setHumidity(double value)
{
    humidity = value;
}

double ForecastMain::getTemp_kf() const
{
    return temp_kf;
}

void ForecastMain::setTemp_kf(double value)
{
    temp_kf = value;
}

void ForecastMain::setPressure(double value)
{
    pressure = value;
}

void ForecastMain::setSea_level(double value)
{
    sea_level = value;
}
