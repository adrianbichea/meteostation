#ifndef CITYWEATHER_H
#define CITYWEATHER_H

#include <QString>
#include "Coord/coord.h"
#include "Weather/weather.h"
#include"SysCountry/syscountry.h"
#include"TempMain/tempmain.h"
#include"Wind/wind.h"
#include"Clouds/clouds.h"

class CityWeather
{
public:
    CityWeather();
    ~CityWeather();

    int getId() const;
    void setId(int value);

    QString getName() const;
    void setName(const QString &value);

    Coord *getCoord() const;
    void setCoord(Coord *value);

    Weather *getWeather() const;
    void setWeather(Weather *value);

    TempMain *getWmain() const;
    void setWmain(TempMain *value);

    long getDt() const;
    void setDt(long value);

    Wind *getWind() const;
    void setWind(Wind *value);

    SysCountry *getSys() const;
    void setSys(SysCountry *value);

    double getRain() const;
    void setRain(double value);

    Clouds *getClouds() const;
    void setClouds(Clouds *value);

    QString getBase() const;
    void setBase(const QString &base);

    int getVisibility() const;
    void setVisibility(int value);

    int getTimezone() const;
    void setTimezone(int value);

    int getCod() const;
    void setCod(int value);

private:
    Coord* coord;
    Weather* weather;
    QString _base;
    TempMain* wmain;
    int visibility;
    Wind* wind;
    double rain;
    Clouds* clouds;
    long dt;
    SysCountry* sys;
    int timezone;
    int id;
    QString name;
    int cod;
};

#endif // CITYWEATHER_H
