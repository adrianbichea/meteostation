#ifndef FORECASTMAIN_H
#define FORECASTMAIN_H


class ForecastMain
{
public:
    ForecastMain();

    double getTemp() const;
    void setTemp(double value);

    double getFeels_like() const;
    void setFeels_like(double value);

    double getTemp_min() const;
    void setTemp_min(double value);

    double getTemp_max() const;
    void setTemp_max(double value);

    double getPressure() const;

    double getSea_level() const;

    double getGrnd_level() const;
    void setGrnd_level(double value);

    double getHumidity() const;
    void setHumidity(double value);

    double getTemp_kf() const;
    void setTemp_kf(double value);

    void setPressure(double value);

    void setSea_level(double value);

private:
    double temp;
    double feels_like;
    double temp_min;
    double temp_max;
    double pressure;
    double sea_level;
    double grnd_level;
    double humidity;
    double temp_kf;
};

#endif // FORECASTMAIN_H
