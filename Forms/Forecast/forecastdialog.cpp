
#include "forecastdialog.h"
#include "ui_forecastdialog.h"
#include "Utils/utils.h"


ForecastDialog::ForecastDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ForecastDialog)
{
    ui->setupUi(this);
    this->showMaximized();
}

ForecastDialog::~ForecastDialog()
{
    delete chartViewTemp;
    delete chartViewPerc;
    delete chartViewWind;
    delete ui;
}

void ForecastDialog::ShowChart(QList<ForecastObject *> *forecastObjects, bool celsius)
{
    if (forecastObjects == nullptr)
    {
        return;
    }

    CreateChartTemp(forecastObjects, celsius);
    CreateChartPerc(forecastObjects);
    CreateChartWind(forecastObjects);

    ui->frameTemp->addWidget(chartViewTemp);
    ui->framePercent->addWidget(chartViewPerc);
    ui->frameWind->addWidget(chartViewWind);
}

void ForecastDialog::CreateChartTemp(QList<ForecastObject *> *forecastObjects, bool celsius)
{
    double minTemp = 100;
    double maxTemp = -100;

    QSplineSeries *series = new QSplineSeries();
    for (int i(0); i < forecastObjects->count(); i++)
    {
        QDateTime momentInTime;

        momentInTime.setTime_t(forecastObjects->at(i)->getDt());
        double temp = Utils::KelvinTo(forecastObjects->at(i)->getForecastMain()->getTemp(), celsius);
        series->append(momentInTime.toMSecsSinceEpoch(), temp);
        if (minTemp > temp)
        {
            minTemp = temp;
        }
        if (maxTemp < temp)
        {
            maxTemp = temp;
        }
    }

    minTemp--;
    maxTemp++;
    QChart *chart = new QChart();
    chart->addSeries(series);
    chart->legend()->hide();
    chart->setTitle(tr("Forecast for 5 days"));

    QDateTimeAxis *axisX = new QDateTimeAxis;
    axisX->setTickCount(9);
    axisX->setFormat("dd-MM HH:mm");
    axisX->setTitleText(tr("Date"));
    chart->addAxis(axisX, Qt::AlignBottom);
    series->attachAxis(axisX);

    QValueAxis *axisY = new QValueAxis;
    axisY->setLabelFormat("%i");
    axisY->setTitleText(tr("Temperature"));
    chart->addAxis(axisY, Qt::AlignLeft);
    chart->setAnimationOptions(QChart::SeriesAnimations);
    series->attachAxis(axisY);
    auto yAxis = chart->axes(Qt::Vertical);
    yAxis.back()->setRange(minTemp, maxTemp);

    chartViewTemp = new QChartView(chart);
    chartViewTemp->setRenderHint(QPainter::Antialiasing);
}

void ForecastDialog::CreateChartWind(QList<ForecastObject *> *forecastObjects)
{
    double maxSpeed = 0;
    QSplineSeries *series = new QSplineSeries();
    for (int i(0); i < forecastObjects->count(); i++)
    {
        QDateTime momentInTime;

        momentInTime.setTime_t(forecastObjects->at(i)->getDt());
        double wind = forecastObjects->at(i)->getWind()->getSpeed();

        series->append(momentInTime.toMSecsSinceEpoch(), wind);
        if (maxSpeed < wind)
        {
            maxSpeed = wind;
        }
    }
    series->setOpacity(1);
    maxSpeed++;

    QChart *chart = new QChart();
    chart->addSeries(series);
    chart->legend()->hide();
    chart->setTitle(tr("Wind"));

    QDateTimeAxis *axisX = new QDateTimeAxis;
    axisX->setTickCount(9);
    axisX->setFormat("dd-MM HH:mm");
    axisX->setTitleText(tr("Date"));
    chart->addAxis(axisX, Qt::AlignBottom);
    series->attachAxis(axisX);

    QValueAxis *axisY = new QValueAxis;
    axisY->setLabelFormat("%i");
    axisY->setTitleText(tr("Wind speed"));
    chart->addAxis(axisY, Qt::AlignLeft);
    chart->setAnimationOptions(QChart::SeriesAnimations);
    series->attachAxis(axisY);

    auto yAxis = chart->axes(Qt::Vertical);
    yAxis.back()->setRange(0, maxSpeed);

    chartViewWind = new QChartView(chart);
    chartViewWind->setRenderHint(QPainter::Antialiasing);
}

void ForecastDialog::CreateChartPerc(QList<ForecastObject *> *forecastObjects)
{
    QLineSeries *seriesUp = new QLineSeries();
    QLineSeries *seriesDown = new QLineSeries();
    for (int i(0); i < forecastObjects->count(); i++)
    {
        QDateTime momentInTime;

        momentInTime.setTime_t(forecastObjects->at(i)->getDt());
        double pop = forecastObjects->at(i)->getPop() * 100.0;
        qDebug() << forecastObjects->at(i)->getPop() << " " << pop;
        seriesUp->append(momentInTime.toMSecsSinceEpoch(), pop);
        seriesDown->append(momentInTime.toMSecsSinceEpoch(), 0.0);
    }

    QAreaSeries *series = new QAreaSeries(seriesUp, seriesDown);
    series->setName(tr("Precipitation"));
    QPen pen(0x0098cc);
    pen.setWidth(3);
    series->setPen(pen);

    QChart *chart = new QChart();
    chart->addSeries(series);
    chart->legend()->hide();
    chart->setTitle(tr("Precipitation Chance"));

    QDateTimeAxis *axisX = new QDateTimeAxis;
    axisX->setTickCount(9);
    axisX->setFormat("dd-MM HH:mm");
    axisX->setTitleText(tr("Date"));
    chart->addAxis(axisX, Qt::AlignBottom);
    series->attachAxis(axisX);

    QValueAxis *axisY = new QValueAxis;
    axisY->setLabelFormat("%i");
    axisY->setTitleText(tr("Probability of precipitation"));
    chart->addAxis(axisY, Qt::AlignLeft);
    chart->setAnimationOptions(QChart::SeriesAnimations);
    series->attachAxis(axisY);

    auto yAxis = chart->axes(Qt::Vertical);
    yAxis.back()->setRange(0, 100);

    chartViewPerc = new QChartView(chart);
    chartViewPerc->setRenderHint(QPainter::Antialiasing);
}
