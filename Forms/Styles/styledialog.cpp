#include "styledialog.h"
#include "ui_styledialog.h"

#include <QColorDialog>
#include <QFile>
#include <QDebug>

StyleDialog::StyleDialog(QColor upColor, QColor textColor, QColor downColor, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::StyleDialog)
{
    setUpColor(upColor);
    setTextColor(textColor);
    setDownColor(downColor);

    qDebug() << upColor.red() << " " << upColor.green() << " " << upColor.blue() <<  " " << upColor.name();
    qDebug() << textColor.red() << " " << textColor.green() << " " << textColor.blue() <<  " " << textColor.name();
    qDebug() << downColor.red() << " " << downColor.green() << " " << downColor.blue() <<  " " << downColor.name();

    ui->setupUi(this);

    UpdateStyle();
}

StyleDialog::~StyleDialog()
{
    delete ui;
}

QColor StyleDialog::getDownColor() const
{
    return downColor;
}

void StyleDialog::setDownColor(const QColor &value)
{
    downColor = value;
}

QColor StyleDialog::getTextColor() const
{
    return textColor;
}

void StyleDialog::setTextColor(const QColor &value)
{
    textColor = value;
}

QColor StyleDialog::getUpColor() const
{
    return upColor;
}

void StyleDialog::setUpColor(const QColor &value)
{
    upColor = value;
}

void StyleDialog::on_btnUpColor_clicked()
{
    QColorDialog cDialog(getUpColor());
    cDialog.exec();
    setUpColor(cDialog.currentColor());
    UpdateStyle();
}

void StyleDialog::on_btnTextColor_clicked()
{
    QColorDialog cDialog(getTextColor());
    cDialog.exec();
    setTextColor(cDialog.currentColor());
    UpdateStyle();
}

void StyleDialog::on_btnDownColor_clicked()
{
    QColorDialog cDialog(getDownColor());
    cDialog.exec();
    setDownColor(cDialog.currentColor());
    UpdateStyle();
}

QString StyleDialog::getFilename() const
{
    return filename;
}

void StyleDialog::setFilename(const QString &value)
{
    filename = value;
}

void StyleDialog::UpdateStyle()
{
    /*QString infosCSS = getStyleString();
    infosCSS += "\nQFrame#frameSea, #frameCity {\n";
    infosCSS += "    background: qlineargradient( x1:1 y1:0, x2:1 y2:1, stop:0 #" +
                    getUpColor().name() + ", stop:1 #" + getDownColor().name() + ");\n";
    infosCSS += "    border-radius: 15px;\n";
    infosCSS += "}\n\n";

    infosCSS += "QLabel {\n";
    infosCSS += "    color: rgb(" + QString::number(getTextColor().red()) + ", " +
                                    QString::number(getTextColor().green()) + ", " +
                                    QString::number(getTextColor().blue()) + ");\n";
    infosCSS += "    background-color: rgba( 255, 255, 255, 0% );\n";
    infosCSS += "}\n";
    qDebug().noquote() << infosCSS;
    setStyleSheet(infosCSS);*/
    ui->frameTemp->setStyleSheet("    background: qlineargradient( x1:1 y1:0, x2:1 y2:1, stop:0 " +
                                 getUpColor().name() + ", stop:1 " + getDownColor().name() + ");\n    border-radius: 15px;\n");
    ui->label->setStyleSheet("color: rgb(" + QString::number(getTextColor().red()) + ", " +
                             QString::number(getTextColor().green()) + ", " +
                             QString::number(getTextColor().blue()) + ");\n"+
                             "    background-color: rgba( 255, 255, 255, 0% );\n");

    qDebug() << ui->frameTemp->styleSheet();
    qDebug() << ui->label->styleSheet();
}

void StyleDialog::on_btnCancel_clicked()
{
    this->close();
}

void StyleDialog::on_btnSave_clicked()
{
    QFile fPartOfStyle(":/combobox/style.txt");
    if (fPartOfStyle.open(QIODevice::ReadOnly))
    {
        QString fileText = fPartOfStyle.readAll();
        QString infosCSS = "";
        infosCSS += "\nQFrame#frameSea, #frameCity {\n";
        infosCSS += "    background: qlineargradient( x1:1 y1:0, x2:1 y2:1, stop:0 " +
                        getUpColor().name() + ", stop:1 " + getDownColor().name() + ");\n";
        infosCSS += "    border-radius: 15px;\n";
        infosCSS += "}\n\n";

        infosCSS += "QLabel {\n";
        infosCSS += "    color: rgb(" + QString::number(getTextColor().red()) + ", " +
                                        QString::number(getTextColor().green()) + ", " +
                                        QString::number(getTextColor().blue()) + ");\n";
        infosCSS += "    background-color: rgba( 255, 255, 255, 0% );\n";
        infosCSS += "}\n";
        infosCSS += fileText;
        //qDebug().noquote() << infosCSS;
        QFile f(getFilename());
        if (f.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            QTextStream stream(&f);
            stream << infosCSS;
            f.close();
        }
    }
    this->close();
}
