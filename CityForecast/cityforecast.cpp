#include <QDate>
#include "cityforecast.h"
#include "Utils/utils.h"

#include <QDebug>

/*!
 * \brief CityForecast::CityForecast
 */
CityForecast::CityForecast()
{
    city = new CityForecastObject();
    forecastObjects = new QList<ForecastObject*>();
}

/*!
 * \brief CityForecast::~CityForecast
 */
CityForecast::~CityForecast()
{
    delete city;
    for (auto fo : *forecastObjects)
    {
        delete fo;
    }
    delete forecastObjects;
}

/*!
 * \brief CityForecast::getCod
 * \return cod
 */
QString CityForecast::getCod() const
{
    return cod;
}

/*!
 * \brief CityForecast::setCod
 * \param value
 */
void CityForecast::setCod(const QString &value)
{
    cod = value;
}

/*!
 * \brief CityForecast::getMessage
 * \return
 */
int CityForecast::getMessage() const
{
    return message;
}

/*!
 * \brief CityForecast::setMessage
 * \param value
 */
void CityForecast::setMessage(int value)
{
    message = value;
}

/*!
 * \brief CityForecast::getCnt
 * \return
 */
int CityForecast::getCnt() const
{
    return cnt;
}

/*!
 * \brief CityForecast::setCnt
 * \param value
 */
void CityForecast::setCnt(int value)
{
    cnt = value;
}

/*!
 * \brief CityForecast::getForecastObjects
 * \return
 */
QList<ForecastObject *> *CityForecast::getForecastObjects() const
{
    return forecastObjects;
}

/*!
 * \brief CityForecast::getCity
 * \return
 */
CityForecastObject *CityForecast::getCity() const
{
    return city;
}

/*!
 * \brief CityForecast::setCity
 * \param value
 */
void CityForecast::setCity(CityForecastObject *value)
{
    city = value;
}

/*!
 * \brief CityForecast::getForecastTemperature
 * \param day
 * \param celsius
 * \param timezone
 * \return temperature as a string
 */
QString CityForecast::getForecastTemperature(int day, bool celsius, int timezone)
{
    QDateTime firstDate;

    //day starts at 06:00:00 - ends at 18:00:00
    //night starts at 21:00:00 - ends second day at 03:00:00

    double min = 300000.0;
    double max = -3000000.0;
    if (forecastObjects->count() < 0)
    {
        return "- / -";
    }

    firstDate = QDateTime::fromString(forecastObjects->at(0)->getDt_txt(), "yyyy-MM-dd HH:mm:ss");
    firstDate = firstDate.addSecs(timezone - firstDate.offsetFromUtc());

    QDateTime startDate;
    QDateTime stopDate;

    startDate.setDate(firstDate.date());
    startDate = startDate.addDays(day - 1);
    startDate.setTime(QTime::fromString("00:00:00", "HH:mm:ss"));
    startDate = startDate.addSecs(timezone - startDate.offsetFromUtc());
    stopDate.setDate(firstDate.date());
    stopDate = stopDate.addDays(day - 1);
    stopDate.setTime(QTime::fromString("23:59:59", "HH:mm:ss"));
    stopDate = stopDate.addSecs(timezone - stopDate.offsetFromUtc());

    for (auto fo : *forecastObjects)
    {
        QDateTime currentDate;
        currentDate = QDateTime::fromString(fo->getDt_txt(), "yyyy-MM-dd HH:mm:ss");
        currentDate = currentDate.addSecs(timezone - currentDate.offsetFromUtc());
        //qDebug() << currentDate.toString() << " " << QString::number(Utils::KelvinTo(fo->getForecastMain()->getTemp(), celsius), 'f', 1);
        if (currentDate >= startDate && currentDate <= stopDate)
        {
            if (max < fo->getForecastMain()->getTemp())
            {
                max = fo->getForecastMain()->getTemp();
            }
            if (min > fo->getForecastMain()->getTemp())
            {
                min = fo->getForecastMain()->getTemp();
            }
        }
    }
    return QString::number(Utils::KelvinTo(min, celsius), 'f', 1) + "/" +
            QString::number(Utils::KelvinTo(max, celsius), 'f', 1);
}

/*!
 * \brief CityForecast::getDayOfTheWeekAndDayOfMonth
 * \param day
 * \param timezone
 * \return day of the week and day of the month
 */
QString CityForecast::getDayOfTheWeekAndDayOfMonth(int day, int timezone)
{
    QDateTime firstDate;

    firstDate = QDateTime::fromString(forecastObjects->at(0)->getDt_txt(), "yyyy-MM-dd HH:mm:ss");
    firstDate = firstDate.addSecs(timezone - firstDate.offsetFromUtc());
    for (auto fo : *forecastObjects)
    {
        QDateTime currentDate;
        currentDate = QDateTime::fromString(fo->getDt_txt(), "yyyy-MM-dd HH:mm:ss");
        currentDate = currentDate.addSecs(timezone - currentDate.offsetFromUtc());
        if (firstDate.daysTo(currentDate) == day - 1)
        {
            return Utils::dayOfTheWeekToString(currentDate.date().dayOfWeek()) + " " + QString::number(currentDate.date().day());
        }
    }
    return "---";
}

/*!
 * \brief CityForecast::getIcon
 * \param day
 * \param timezone
 * \return
 */
QString CityForecast::getIcon(int day, int timezone)
{
    QDateTime firstDate;

    //day 12:00:00
    //night 00:00:00 next day

    if (forecastObjects->count() < 0)
    {
        return "unknown";
    }

    firstDate = QDateTime::fromString(forecastObjects->at(0)->getDt_txt(), "yyyy-MM-dd HH:mm:ss");
    firstDate = firstDate.addSecs(timezone - firstDate.offsetFromUtc());

    QDateTime searchDate;

    searchDate.setDate(firstDate.date());
    searchDate = searchDate.addDays(day - 1);
    searchDate.setTime(QTime::fromString("12:00:00", "HH:mm:ss"));
    searchDate = searchDate.addSecs(timezone - searchDate.offsetFromUtc());

    QString lastIconFound = "";
    QString icon = "";
    bool lastFound = false;
    for (auto fo : *forecastObjects)
    {
        QDateTime currentDate;
        currentDate = QDateTime::fromString(fo->getDt_txt(), "yyyy-MM-dd HH:mm:ss");
        currentDate = currentDate.addSecs(timezone - currentDate.offsetFromUtc());
        if (searchDate.daysTo(currentDate) == day - 1)
        {
            if (!lastFound && fo->getWeatherObjects()->count() > 0)
            {
                lastIconFound = fo->getWeatherObjects()->at(0)->getIcon();
                lastFound = true;
            }
        }
        if (currentDate <= searchDate)
        {
            if (fo->getWeatherObjects()->count() > 0)
            {
                icon = fo->getWeatherObjects()->at(0)->getIcon();
            }
        }
    }

    if (icon.isEmpty())
    {
        icon = lastIconFound;
    }

    if (icon.isEmpty())
    {
        return "unknown";
    }

    return icon;
}

QString CityForecast::getDescription(int day, int timezone)
{
    QDateTime firstDate;

    //day 12:00:00
    //night 00:00:00 next day

    if (forecastObjects->count() < 0)
    {
        return "unknown";
    }

    firstDate = QDateTime::fromString(forecastObjects->at(0)->getDt_txt(), "yyyy-MM-dd HH:mm:ss");
    firstDate = firstDate.addSecs(timezone - firstDate.offsetFromUtc());

    QDateTime searchDate;

    searchDate.setDate(firstDate.date());
    searchDate = searchDate.addDays(day - 1);
    searchDate.setTime(QTime::fromString("12:00:00", "HH:mm:ss"));
    searchDate = searchDate.addSecs(timezone - searchDate.offsetFromUtc());

    QString lastDescription = "";
    QString description = "";
    for (auto fo : *forecastObjects)
    {
        QDateTime currentDate;
        currentDate = QDateTime::fromString(fo->getDt_txt(), "yyyy-MM-dd HH:mm:ss");
        currentDate = currentDate.addSecs(timezone - currentDate.offsetFromUtc());
        if (searchDate.daysTo(currentDate) == day - 1)
        {
            if (fo->getWeatherObjects()->count() > 0)
            {
                lastDescription = fo->getWeatherObjects()->at(0)->getDescription();
            }
        }
        if (currentDate <= searchDate)
        {
            if (fo->getWeatherObjects()->count() > 0)
            {
                description = fo->getWeatherObjects()->at(0)->getDescription();
            }
        }
    }

    if (description.isEmpty())
    {
        description = lastDescription;
    }

    if (description.isEmpty())
    {
        return "unknown";
    }

    return description;
}

QString CityForecast::getToolTip(int day, bool celsius, int timezone)
{
    QDateTime firstDate;

    QString toolTip = "";

    if (forecastObjects->count() < 0)
    {
        return "--";
    }

    firstDate = QDateTime::fromString(forecastObjects->at(0)->getDt_txt(), "yyyy-MM-dd HH:mm:ss");
    firstDate = firstDate.addSecs(timezone - firstDate.offsetFromUtc());

    QDateTime startDate;
    QDateTime stopDate;

    startDate.setDate(firstDate.date());
    startDate = startDate.addDays(day - 1);
    startDate.setTime(QTime::fromString("00:00:00", "HH:mm:ss"));
    startDate = startDate.addSecs(timezone - startDate.offsetFromUtc());
    stopDate.setDate(firstDate.date());
    stopDate = stopDate.addDays(day - 1);
    stopDate.setTime(QTime::fromString("23:59:59", "HH:mm:ss"));
    stopDate = stopDate.addSecs(timezone - stopDate.offsetFromUtc());

    QString tempMeasurement = "C";
    if (!celsius)
    {
        tempMeasurement = "F";
    }

    bool first = true;
    for (auto fo : *forecastObjects)
    {
        QDateTime currentDate;
        currentDate = QDateTime::fromString(fo->getDt_txt(), "yyyy-MM-dd HH:mm:ss");
        currentDate = currentDate.addSecs(timezone - currentDate.offsetFromUtc());
        if (currentDate >= startDate && currentDate <= stopDate)
        {
            if (!first)
            {
                toolTip += "<br>";
            }
            first = false;
            toolTip += "<b>" + currentDate.toString("HH:mm:ss") + "</b> ";
            double tmp = Utils::KelvinTo(fo->getForecastMain()->getTemp(), celsius);
            toolTip += QString::number(tmp, 'f', 1) + " " + tempMeasurement;
            toolTip += " [<i>" + fo->getWeatherObjects()->at(0)->getDescription() + "</i>]";
        }
    }
    return toolTip;
}


