#include "seaweather.h"

#include <QHostAddress>
#include <QApplication>
#include <QDateTime>

SeaWeather::SeaWeather(QString ip, int port)
{
    setSensorName("***");
    setHumidity(0);
    setAltitude(0);
    setPressure(0);
    setTemperature(0);
    setTimestamp(QDateTime::currentDateTime().toTime_t());
    connect(&socket, &QTcpSocket::readyRead, this, &SeaWeather::onReadyRead);
    timer = new QTimer();
    timer->setInterval(1000);
    connect(timer, &QTimer::timeout, this, &SeaWeather::onTimeOutReached);

    ipConnection = ip;
    portConnection = port;
}

SeaWeather::~SeaWeather()
{
    delete timer;
}

QString SeaWeather::getSensorName() const
{
    return sensorName;
}

void SeaWeather::setSensorName(const QString &value)
{
    sensorName = value;
}

double SeaWeather::getHumidity() const
{
    return humidity;
}

void SeaWeather::setHumidity(double value)
{
    humidity = value;
}

double SeaWeather::getPressure() const
{
    return pressure;
}

void SeaWeather::setPressure(double value)
{
    pressure = value;
}

double SeaWeather::getTemperature(bool typeTemp) const
{
    return typeTemp ? temperature : (temperature * 1.8) + 32.0;
}

void SeaWeather::setTemperature(double value)
{
    temperature = value;
}

double SeaWeather::getAltitude() const
{
    return altitude;
}

void SeaWeather::setAltitude(double value)
{
    altitude = value;
}

long SeaWeather::getTimestamp() const
{
    return timestamp;
}

void SeaWeather::setTimestamp(double value)
{
    timestamp = value;
}

void SeaWeather::ReadWeather(bool isTCP)
{
    updating = true;
    countTimer = 0;
    timer->start();
    lastConnectionTimeOut = false;
    if (isTCP)
    {
        socket.connectToHost(QHostAddress(ipConnection), portConnection);
    }
}

void SeaWeather::onReadyRead()
{
    QByteArray datas = socket.readAll();
    UpdateData(datas);
}

void SeaWeather::onTimeOutReached()
{
    if (countTimer++ > timeoutTCP)
    {
        lastConnectionTimeOut = true;
        if (socket.isOpen())
        {
            socket.disconnectFromHost();
        }
        countTimer = 0;
        updating = false;
        timer->stop();
    }

}

int SeaWeather::getPortConnection() const
{
    return portConnection;
}

void SeaWeather::setPortConnection(int value)
{
    portConnection = value;
}

QString SeaWeather::getIpConnection() const
{
    return ipConnection;
}

void SeaWeather::setIpConnection(const QString &value)
{
    ipConnection = value;
}

bool SeaWeather::getLastConnectionTimeOut() const
{
    return lastConnectionTimeOut;
}

bool SeaWeather::getUpdating() const
{
    return updating;
}

void SeaWeather::UpdateData(QByteArray response_data)
{
    timer->stop();
    //Conversion du ByteArryay en Json
    QJsonDocument jsonResponse = QJsonDocument::fromJson(response_data);

    QJsonObject jsonObject = jsonResponse.object();

    setSensorName(jsonObject["sensor"].toString());
    setHumidity(jsonObject["humidity"].toDouble());
    setPressure(jsonObject["pressure"].toDouble());
    setTemperature(jsonObject["temperature"].toDouble());
    setAltitude(jsonObject["altitude"].toDouble());
    setTimestamp(jsonObject["timestamp"].toInt());

    updating = false;
}
