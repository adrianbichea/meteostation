<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR" sourcelanguage="en">
<context>
    <name>ForecastDialog</name>
    <message>
        <location filename="Forms/Forecast/forecastdialog.cpp" line="67"/>
        <source>Forecast for 5 days</source>
        <translation>Prevision méteo à 5 jours</translation>
    </message>
    <message>
        <location filename="Forms/Forecast/forecastdialog.cpp" line="72"/>
        <location filename="Forms/Forecast/forecastdialog.cpp" line="117"/>
        <location filename="Forms/Forecast/forecastdialog.cpp" line="164"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="Forms/Forecast/forecastdialog.cpp" line="78"/>
        <source>Temperature</source>
        <translation>Température</translation>
    </message>
    <message>
        <location filename="Forms/Forecast/forecastdialog.cpp" line="112"/>
        <source>Wind</source>
        <translation>Vent</translation>
    </message>
    <message>
        <location filename="Forms/Forecast/forecastdialog.cpp" line="123"/>
        <source>Wind speed</source>
        <translation>Vitesse du vent</translation>
    </message>
    <message>
        <location filename="Forms/Forecast/forecastdialog.cpp" line="151"/>
        <source>Precipitation</source>
        <translation>Précipitation</translation>
    </message>
    <message>
        <location filename="Forms/Forecast/forecastdialog.cpp" line="159"/>
        <source>Precipitation Chance</source>
        <translation>Chance de précipitation</translation>
    </message>
    <message>
        <location filename="Forms/Forecast/forecastdialog.cpp" line="170"/>
        <source>Probability of precipitation</source>
        <translation>Probabilité de précipitation</translation>
    </message>
    <message>
        <location filename="Forms/Forecast/forecastdialog.ui" line="14"/>
        <source>Forecast charts</source>
        <translation>Graphiques de prévision</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="32"/>
        <source>City name here</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="120"/>
        <source>Paris, FR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="199"/>
        <location filename="mainwindow.ui" line="773"/>
        <location filename="mainwindow.ui" line="1023"/>
        <location filename="mainwindow.ui" line="1273"/>
        <location filename="mainwindow.ui" line="1529"/>
        <location filename="mainwindow.ui" line="1788"/>
        <source>25</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="275"/>
        <source>Wheather type (Sunny.. etc..)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="326"/>
        <source>Next update in: 04:36</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="575"/>
        <source>Min 10</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="521"/>
        <source>Pressure 125000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="554"/>
        <source>Max 35</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="464"/>
        <source>Wind speed 40</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Meteo station</source>
        <translation>Station météo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="151"/>
        <source>Location time</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="377"/>
        <source>Feels like 60000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="500"/>
        <source>Humidity 80%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="671"/>
        <location filename="mainwindow.ui" line="920"/>
        <location filename="mainwindow.ui" line="1170"/>
        <location filename="mainwindow.ui" line="1426"/>
        <location filename="mainwindow.ui" line="1685"/>
        <source>{DoW} {Day}</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="838"/>
        <location filename="mainwindow.ui" line="1088"/>
        <location filename="mainwindow.ui" line="1338"/>
        <location filename="mainwindow.ui" line="1594"/>
        <location filename="mainwindow.ui" line="1853"/>
        <source>16</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1911"/>
        <location filename="mainwindow.cpp" line="24"/>
        <location filename="mainwindow.cpp" line="355"/>
        <source>Sea weather</source>
        <translation>Météo de la mer</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1934"/>
        <location filename="mainwindow.cpp" line="173"/>
        <source>Connection timeout !</source>
        <translation>Délai de connection dépassé!</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2021"/>
        <source>Sensor name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2107"/>
        <source>Humidity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2193"/>
        <source>Pressure</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2279"/>
        <source>Temperature</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2365"/>
        <source>Altitude</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2451"/>
        <source>Time</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2550"/>
        <source>Next read in:</source>
        <translation>Prochaine lecture:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2589"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2600"/>
        <location filename="mainwindow.cpp" line="22"/>
        <location filename="mainwindow.cpp" line="353"/>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2605"/>
        <location filename="mainwindow.cpp" line="23"/>
        <location filename="mainwindow.cpp" line="354"/>
        <source>View Forecast</source>
        <translation>Voir les prévisions</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="140"/>
        <source>Sensor name: </source>
        <translation>Nom sensor: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="141"/>
        <location filename="mainwindow.cpp" line="239"/>
        <source>Humidity: </source>
        <translation>Humidité: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="142"/>
        <location filename="mainwindow.cpp" line="240"/>
        <source>Pressure: </source>
        <translation>Pression: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="142"/>
        <location filename="mainwindow.cpp" line="240"/>
        <source> mbar</source>
        <translation> mbar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="144"/>
        <source>Temperature: </source>
        <translation>Température: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="155"/>
        <source>Altitude: </source>
        <translation>Altitude: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="160"/>
        <location filename="mainwindow.cpp" line="164"/>
        <source>Local time: </source>
        <oldsource>Timestamp: </oldsource>
        <translation>Heure locale: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="166"/>
        <source>Time until next update: </source>
        <translation>Prochaine mise à jour en: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="179"/>
        <source>City not found!</source>
        <translation>Ville introuvable!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="188"/>
        <location filename="mainwindow.cpp" line="192"/>
        <location filename="mainwindow.cpp" line="265"/>
        <source>Last reading: </source>
        <translation>Dernière lecture: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="219"/>
        <source>Unknown</source>
        <translation>Inconnu</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="236"/>
        <source>Feels like: </source>
        <translation>Ressenti: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="237"/>
        <source>Min: </source>
        <translation>Min: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="238"/>
        <source>Max: </source>
        <translation>Max: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="241"/>
        <source>Wind speed: </source>
        <translation>Vitesse du vent: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="241"/>
        <source> m/sec</source>
        <translation> m/sec</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="251"/>
        <source>Weather type: </source>
        <translation>Temps: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="257"/>
        <source> - doesnt exist!</source>
        <translation> - n&apos;existe pas!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="331"/>
        <source>Next update in: </source>
        <translation>Mise à jour dans: </translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="20"/>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="61"/>
        <source>General Options</source>
        <translation>Options Générales</translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="73"/>
        <source>Language</source>
        <translation>Langage</translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="85"/>
        <source>EN</source>
        <translation>EN</translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="98"/>
        <source>FR</source>
        <translation>FR</translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="112"/>
        <source>Temperature view</source>
        <translation>Température</translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="124"/>
        <source>Fahrenheit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="137"/>
        <source>Celsius</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="151"/>
        <source>Format hour</source>
        <translation>Format de l&apos;heure</translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="163"/>
        <source>12H</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="176"/>
        <source>24H</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="190"/>
        <source>Day/Night</source>
        <translation>Jour/Nuit</translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="202"/>
        <source>Night</source>
        <translation>Nuit</translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="215"/>
        <source>Day</source>
        <translation>Jour</translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="228"/>
        <source>Automatic</source>
        <translation>Automatique</translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="240"/>
        <location filename="Forms/Preferences/preferencesdialog.cpp" line="75"/>
        <location filename="Forms/Preferences/preferencesdialog.cpp" line="165"/>
        <source>BME280</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="264"/>
        <source>Update time BME280</source>
        <translation>Durée de mise à jour BME280</translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="302"/>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="565"/>
        <source>sec.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="327"/>
        <source>IP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="352"/>
        <source>Port</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="389"/>
        <location filename="Forms/Preferences/preferencesdialog.cpp" line="71"/>
        <location filename="Forms/Preferences/preferencesdialog.cpp" line="161"/>
        <source>Font</source>
        <translation>Police</translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="401"/>
        <source>Day StyleSheet</source>
        <translation>Style Mode Jour</translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="414"/>
        <source>Day Font</source>
        <translation>Police Mode Jour</translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="427"/>
        <source>Night Font</source>
        <translation>Police Mode Nuit</translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="453"/>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="466"/>
        <source>Style</source>
        <translation>Style</translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="497"/>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="529"/>
        <source>Font Test</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="440"/>
        <source>Night StyleSheet</source>
        <translation>Style Mode Nuit</translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="541"/>
        <location filename="Forms/Preferences/preferencesdialog.cpp" line="79"/>
        <location filename="Forms/Preferences/preferencesdialog.cpp" line="169"/>
        <source>HTTP/HTTPS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="621"/>
        <source>Update time HTTP</source>
        <translation>Durée de mise à jour HTTP</translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="646"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="700"/>
        <source>City</source>
        <translation>Ville</translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="725"/>
        <source>Country</source>
        <translation>Pays</translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.ui" line="794"/>
        <source>App Id</source>
        <translation></translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.cpp" line="67"/>
        <location filename="Forms/Preferences/preferencesdialog.cpp" line="157"/>
        <source>General options</source>
        <translation>Options générales</translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.cpp" line="118"/>
        <location filename="Forms/Preferences/preferencesdialog.cpp" line="129"/>
        <source>Open Stylesheet</source>
        <translation>Stylesheet</translation>
    </message>
    <message>
        <location filename="Forms/Preferences/preferencesdialog.cpp" line="118"/>
        <location filename="Forms/Preferences/preferencesdialog.cpp" line="129"/>
        <source>Stylesheet files (*.qcc *.css)</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="Utils/utils.cpp" line="38"/>
        <source>Monday</source>
        <translation>Lundi</translation>
    </message>
    <message>
        <location filename="Utils/utils.cpp" line="41"/>
        <source>Tuesday</source>
        <translation>Mardi</translation>
    </message>
    <message>
        <location filename="Utils/utils.cpp" line="44"/>
        <source>Wednesday</source>
        <translation>Mercredi</translation>
    </message>
    <message>
        <location filename="Utils/utils.cpp" line="47"/>
        <source>Thursday</source>
        <translation>Jeudi</translation>
    </message>
    <message>
        <location filename="Utils/utils.cpp" line="50"/>
        <source>Friday</source>
        <translation>Vendredi</translation>
    </message>
    <message>
        <location filename="Utils/utils.cpp" line="53"/>
        <source>Saturday</source>
        <translation>Samedi</translation>
    </message>
    <message>
        <location filename="Utils/utils.cpp" line="56"/>
        <source>Sunday</source>
        <translation>Dimanche</translation>
    </message>
</context>
<context>
    <name>StyleDialog</name>
    <message>
        <location filename="Forms/Styles/styledialog.ui" line="14"/>
        <source>Stylesheet options</source>
        <translation>Options de style</translation>
    </message>
    <message>
        <location filename="Forms/Styles/styledialog.ui" line="37"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="Forms/Styles/styledialog.ui" line="57"/>
        <source>Save</source>
        <translation>Sauvegarder</translation>
    </message>
    <message>
        <location filename="Forms/Styles/styledialog.ui" line="100"/>
        <source>Text</source>
        <translation>Text</translation>
    </message>
    <message>
        <location filename="Forms/Styles/styledialog.ui" line="132"/>
        <location filename="Forms/Styles/styledialog.ui" line="171"/>
        <location filename="Forms/Styles/styledialog.ui" line="210"/>
        <source>Change</source>
        <translation>Changer</translation>
    </message>
</context>
</TS>
