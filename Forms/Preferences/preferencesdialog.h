#ifndef PREFERENCESDIALOG_H
#define PREFERENCESDIALOG_H

#include <QDialog>
#include <QListWidgetItem>
#include <QCloseEvent>

#include "Preferences/preferences.h"
#include "Utils/CountryInfos/countriesmap.h"

namespace Ui {
class PreferencesDialog;
}

class PreferencesDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PreferencesDialog(QWidget *parent = nullptr);
    ~PreferencesDialog();

private slots:
    void on_btnDayFont_clicked();

    void on_btnSDayStyle_clicked();

    void on_btnNightStyle_clicked();

    void on_btnNightFont_clicked();

    void on_listOptions_itemClicked(QListWidgetItem *item);

    void on_btnCreateStyleNight_clicked();

    void on_btnCreateStyleDay_clicked();

private:
    Ui::PreferencesDialog *ui;

    Preferences p;

    void closeEvent(QCloseEvent *bar);
    CountriesMap countriesMap;

    void OpenStyleDialog(QString filename);

    void UpdateStyleSheet();
    QString GetFilename(QString filenameStyleSheet);
};

#endif // PREFERENCESDIALOG_H
