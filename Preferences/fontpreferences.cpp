#include <QDebug>
#include "fontpreferences.h"

FontPreferences::FontPreferences()
{
    setFontName("Arial");
    setBold(false);
    setItalic(false);
    setUnderline(false);
    setSize(12);
}

FontPreferences::FontPreferences(QString fontName, bool bold, bool italic, bool underline, int size)
{
    setFontName(fontName);
    setBold(bold);
    setItalic(italic);
    setUnderline(underline);
    setSize(size);
}

FontPreferences::~FontPreferences()
{

}

QString FontPreferences::getFontName() const
{
    return fontName;
}

void FontPreferences::setFontName(const QString &value)
{
    fontName = value;
}

bool FontPreferences::getBold() const
{
    return bold;
}

void FontPreferences::setBold(bool value)
{
    bold = value;
}

bool FontPreferences::getItalic() const
{
    return italic;
}

void FontPreferences::setItalic(bool value)
{
    italic = value;
}

bool FontPreferences::getUnderline() const
{
    return underline;
}

void FontPreferences::setUnderline(bool value)
{
    underline = value;
}

int FontPreferences::getSize() const
{
    return size;
}

void FontPreferences::setSize(int value)
{
    size = value;
}

QString FontPreferences::toString()
{
    qDebug() << getFontName();
    return getFontName() + ";" +
            QString::number(getBold()) + ";" +
            QString::number(getItalic()) + ";" +
            QString::number(getUnderline()) + ";" +
            QString::number(getSize());
}

void FontPreferences::fromString(QString str)
{
    auto listProp = str.split(";");

    if (listProp.size() != 5)
    {
        return;
    }
    setFontName(listProp.at(0));
    setBold(listProp.at(1) != "0");
    setItalic(listProp.at(2) != "0");
    setUnderline(listProp.at(3) != "0");
    setSize(listProp.at(4).toInt());
}

QFont FontPreferences::getFont()
{
    QFont font(getFontName(), getSize());
    font.setBold(getBold());
    font.setItalic(getItalic());
    font.setUnderline(getUnderline());
    return font;
}

QString FontPreferences::getStyleSheet()
{
    QString infosCSS = "QLabel {\n";
    infosCSS += "font-family: " + getFontName() + ";\n";
    infosCSS += "font-size: " + QString::number(getSize()) + "pt;\n";
    if (getBold())
    {
        infosCSS += "font-weight: bold;\n";
    }
    if (getItalic())
    {
        infosCSS += "font-style: italic;\n";
    }
    if (getUnderline())
    {
        infosCSS += "text-decoration: underline;\n";
    }
    infosCSS += "}\n";
    return infosCSS;
}
