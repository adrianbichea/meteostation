#include "cityforecastobject.h"

CityForecastObject::CityForecastObject()
{
    coord = new Coord(0, 0);
}

CityForecastObject::~CityForecastObject()
{
    delete coord;
}

int CityForecastObject::getId() const
{
    return id;
}

void CityForecastObject::setId(int value)
{
    id = value;
}

Coord *CityForecastObject::getCoord() const
{
    return coord;
}

void CityForecastObject::setCoord(Coord *value)
{
    coord = value;
}

QString CityForecastObject::getCountry() const
{
    return country;
}

void CityForecastObject::setCountry(const QString &value)
{
    country = value;
}

int CityForecastObject::getPopulation() const
{
    return population;
}

void CityForecastObject::setPopulation(int value)
{
    population = value;
}

int CityForecastObject::getTimezone() const
{
    return timezone;
}

void CityForecastObject::setTimezone(int value)
{
    timezone = value;
}

long CityForecastObject::getSunrise() const
{
    return sunrise;
}

void CityForecastObject::setSunrise(long value)
{
    sunrise = value;
}

long CityForecastObject::getSunset() const
{
    return sunset;
}

void CityForecastObject::setSunset(long value)
{
    sunset = value;
}

QString CityForecastObject::getName() const
{
    return name;
}

void CityForecastObject::setName(const QString &value)
{
    name = value;
}
