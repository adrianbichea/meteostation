#include "syscountry.h"

SysCountry::SysCountry(QString country)
{
    setCountry(country);
}

SysCountry::~SysCountry()
{

}

QString SysCountry::getCountry() const
{
    return country;
}

void SysCountry::setCountry(const QString &value)
{
    country = value;
}

int SysCountry::getCountryType() const
{
    return countryType;
}

void SysCountry::setCountryType(int value)
{
    countryType = value;
}

int SysCountry::getId() const
{
    return id;
}

void SysCountry::setId(int value)
{
    id = value;
}

long SysCountry::getSunrise() const
{
    return sunrise;
}

void SysCountry::setSunrise(long value)
{
    sunrise = value;
}

long SysCountry::getSunset() const
{
    return sunset;
}

void SysCountry::setSunset(long value)
{
    sunset = value;
}
