#include "coord.h"

Coord::Coord(double lat, double lon)
{
    setLat(lat);
    setLon(lon);
}

Coord::~Coord() {}

double Coord::getLat() const
{
    return lat;
}

void Coord::setLat(double value)
{
    lat = value;
}

double Coord::getLon() const
{
    return lon;
}

void Coord::setLon(double value)
{
    lon = value;
}
