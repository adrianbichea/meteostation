#ifndef TEMPMAIN_H
#define TEMPMAIN_H


class TempMain
{
public:
    TempMain(double,double,double,double,int,int);
    ~TempMain();

    double getTemp(bool celsius) const;
    void setTemp(double value);

    double getFeels_like(bool celsius) const;
    void setFeels_like(double value);

    double getTemp_min(bool celsius) const;
    void setTemp_min(double value);

    double getTemp_max(bool celsius) const;
    void setTemp_max(double value);

    int getPressure() const;
    void setPressure(int value);

    int getHumidity() const;
    void setHumidity(int value);

private:
    double temp;
    double feels_like;
    double temp_min;
    double temp_max;
    int pressure;
    int humidity;

    double KelvinToMyUnit(double value, bool celsius) const;
};

#endif // TEMPMAIN_H
