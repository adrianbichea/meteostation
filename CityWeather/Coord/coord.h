#ifndef COORD_H
#define COORD_H


class Coord
{
public:
    Coord(double, double);
    ~Coord();

    double getLat() const;
    void setLat(double value);

    double getLon() const;
    void setLon(double value);

private:
    double lat;
    double lon;
};

#endif // COORD_H
