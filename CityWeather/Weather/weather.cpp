#include "weather.h"

Weather::Weather()
{

}

Weather::~Weather()
{
    for (auto wo : weatherObjects)
    {
        delete wo;
    }
    weatherObjects.clear();
}

QList<WeatherObject *> Weather::getWeatherObjects() const
{
    return weatherObjects;

}

void Weather::setWeatherObjects(const QList<WeatherObject *> &value)
{
    weatherObjects = value;
}

void Weather::AddWeatherObject(int id, QString woMain, QString description, QString icon)
{
    weatherObjects.push_back(new WeatherObject(id, woMain, description, icon));
}
