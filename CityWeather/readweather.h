#ifndef READWEATHER_H
#define READWEATHER_H

#include <QTimer>
#include <QObject>
#include"cityweather.h"

class ReadWeather : public QObject
{
public:
    ReadWeather();
    ~ReadWeather();
    CityWeather* Read(QString city, QString url, QString appId, QString lang);

    bool ReadyToRead();

private slots:
    void on_TimerNetwork_Tick();

private:
    QTimer* timerNetwork;
};

#endif // READWEATHER_H
