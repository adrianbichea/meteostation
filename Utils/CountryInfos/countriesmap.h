#ifndef COUNTRIESMAP_H
#define COUNTRIESMAP_H
#include"countryinfos.h"

#include <QMap>

class CountriesMap
{
public:
    CountriesMap();
    void PopulateMap(QString filename);
    QString getAlpha2(QString countryLongName);
    QString getValueFromKey(QString key);
    QMap<QString, QString> getMap() const;

private:
    QMap<QString,QString> map;
    void AddToMap(QString key,QString value);
};

#endif // COUNTRIESMAP_H
