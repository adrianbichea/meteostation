#ifndef FORECASTOBJECT_H
#define FORECASTOBJECT_H

#include <QString>
#include <QList>
#include "CityWeather/Weather/WeatherObject/weatherobject.h"
#include "CityWeather/Wind/wind.h"
#include "CityForecast/ForecastMain/forecastmain.h"
#include "CityForecast/ForecastClouds/forecastclouds.h"
#include "CityForecast/ForecastSys/forecastsys.h"

class ForecastObject
{
public:
    ForecastObject();
    ~ForecastObject();

    long getDt() const;
    void setDt(long value);

    ForecastMain *getForecastMain() const;
    void setForecastMain(ForecastMain *value);

    QList<WeatherObject *> *getWeatherObjects() const;

    ForecastClouds *getForecastClouds() const;
    void setForecastClouds(ForecastClouds *value);

    Wind *getWind() const;
    void setWind(Wind *value);

    int getVisibility() const;
    void setVisibility(int value);

    double getPop() const;
    void setPop(double value);

    ForecastSys *getSys() const;
    void setSys(ForecastSys *value);

    QString getDt_txt() const;
    void setDt_txt(const QString &value);

private:
    long dt;
    ForecastMain* forecastMain;
    QList<WeatherObject*> *weatherObjects;
    ForecastClouds* forecastClouds;
    Wind* wind;
    int visibility;
    double pop;
    ForecastSys* sys;
    QString dt_txt;
};

#endif // FORECASTOBJECT_H
