#ifndef FORECASTREAD_H
#define FORECASTREAD_H

#include "cityforecast.h"

class ForecastRead
{
public:
    ForecastRead();
    CityForecast* Read(QString address, QString city, QString appId, QString lang);
};

#endif // FORECASTREAD_H
