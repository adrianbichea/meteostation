#ifndef CLOUDS_H
#define CLOUDS_H


class Clouds
{
public:
    Clouds(int);
    ~Clouds();
    int getAll() const;
    void setAll(int value);

private:
    int all;
};

#endif // CLOUDS_H
