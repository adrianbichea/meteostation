#ifndef SYSCOUNTRY_H
#define SYSCOUNTRY_H

#include <QString>



class SysCountry
{
public:
    SysCountry(QString);
    ~SysCountry();
    QString getCountry() const;
    void setCountry(const QString &value);

    int getCountryType() const;
    void setCountryType(int value);

    int getId() const;
    void setId(int value);

    long getSunrise() const;
    void setSunrise(long value);

    long getSunset() const;
    void setSunset(long value);

private:
    int countryType;
    int id;
    QString country;
    long sunrise;
    long sunset;
};

#endif // SYSCOUNTRY_H
