#ifndef UTILS_H
#define UTILS_H

#include <QString>
#include <QObject>

#include "Preferences/preferences.h"

class Utils
{
public:
    Utils();

    static QString getNumberWithDigits(int number, int digits);
    static double KelvinTo(double value, bool celsius);
    static QString dayOfTheWeekToString(int value);

    static QString getLanguage(eLanguage lang);
};

#endif // UTILS_H
