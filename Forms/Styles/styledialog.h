#ifndef STYLEDIALOG_H
#define STYLEDIALOG_H

#include <QDialog>
#include <QColor>

namespace Ui {
class StyleDialog;
}

class StyleDialog : public QDialog
{
    Q_OBJECT

public:
    explicit StyleDialog(QColor upColor, QColor textColor, QColor downColor, QWidget *parent = nullptr);
    ~StyleDialog();

    QColor getUpColor() const;
    void setUpColor(const QColor &value);

    QColor getTextColor() const;
    void setTextColor(const QColor &value);

    QColor getDownColor() const;
    void setDownColor(const QColor &value);

    QString getFilename() const;
    void setFilename(const QString &value);

private slots:
    void on_btnUpColor_clicked();

    void on_btnTextColor_clicked();

    void on_btnDownColor_clicked();

    void on_btnCancel_clicked();

    void on_btnSave_clicked();

private:
    Ui::StyleDialog *ui;

    QColor upColor;
    QColor textColor;
    QColor downColor;
    QString filename;

    void UpdateStyle();
};

#endif // STYLEDIALOG_H
