#include "countryinfos.h"

CountryInfos::CountryInfos()
{
	this->setName("");this->setAlpha2("");this->setCountrycode("");
}

QString CountryInfos::getName() const
{
    return name;
}

void CountryInfos::setName(const QString &value)
{
    name = value;
}


QString CountryInfos::getCountrycode() const
{
    return countrycode;
}

void CountryInfos::setCountrycode(const QString &value)
{
    countrycode = value;
}

QString CountryInfos::getAlpha2() const
{
    return alpha2;
}

void CountryInfos::setAlpha2(const QString &value)
{
    alpha2 = value;
}
