#ifndef WIND_H
#define WIND_H


class Wind
{
public:
    Wind(double,int);
    ~Wind();

    int getDeg() const;
    void setDeg(double value);

    double getSpeed() const;
    void setSpeed(int value);

private:
    int deg;
    double speed;

};

#endif // WIND_H
