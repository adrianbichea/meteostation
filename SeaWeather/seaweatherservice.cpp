#include "seaweatherservice.h"
#include "Utils/utils.h"

SeaWeatherService::SeaWeatherService(int timeRefresh, QString ip, int port, bool isTCP)
{
    isTCPConnection = isTCP;
    timeOutRefresh = timeRefresh;//will be an option
    currentTimeOut = 0;
    seaWeather = new SeaWeather(ip, port);
    timer = new QTimer();
    timer->setInterval(1000);
    connect(timer, &QTimer::timeout, this, &SeaWeatherService::Timer_Tick);
    ReadData();
    timer->start();
}

SeaWeatherService::~SeaWeatherService()
{
    delete timer;
    delete seaWeather;
}

QString SeaWeatherService::getTimeUntilUpdate()
{
    if (seaWeather->getUpdating())
    {
        return " updating ... ";
    }
    int restTime = (timeOutRefresh - currentTimeOut);
    if (restTime < 0)
    {
        restTime = 0;
    }
    int minutes = restTime / 60;
    int seconds = restTime - (minutes * 60);
    return Utils::getNumberWithDigits(minutes, 2) + ":" + Utils::getNumberWithDigits(seconds, 2);
}

void SeaWeatherService::SetNewTimeRefresh(int timeRefresh)
{
    timeOutRefresh = timeRefresh;
}

void SeaWeatherService::Timer_Tick()
{
    if (seaWeather->getUpdating())
    {
        return;
    }
    if (++currentTimeOut >= timeOutRefresh)
    {
        ReadData();
    }
}

bool SeaWeatherService::getIsTCPConnection() const
{
    return isTCPConnection;
}

void SeaWeatherService::setIsTCPConnection(bool value)
{
    isTCPConnection = value;
}


SeaWeather *SeaWeatherService::getSeaWeather() const
{
    return seaWeather;
}

void SeaWeatherService::ReadData()
{
    seaWeather->ReadWeather(isTCPConnection);
    currentTimeOut = 0;
}
