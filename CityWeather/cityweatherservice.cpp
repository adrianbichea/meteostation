#include "cityweatherservice.h"
#include "Utils/utils.h"

#include <QDebug>

CityWeatherService::CityWeatherService(QString city, QString country, QString address, QString appId, int timeToRead, QString lang)
{
    setCity(city);
    setCountry(country);
    setAddressHTTP(address);
    setAppId(appId);
    this->timeToRead = timeToRead;
    currentTimeToRead = 0;

    setLang(lang);

    readWeather = new ReadWeather();
    cityWeather = new CityWeather();
    ForceRead();

    timerCityWeather = new QTimer();
    timerCityWeather->setInterval(1000);
    connect(timerCityWeather, &QTimer::timeout, this, &CityWeatherService::onTimerCityWeather_Tick);
    timerCityWeather->start();
}

QString CityWeatherService::getCity() const
{
    return city;
}

void CityWeatherService::setCity(const QString &value)
{
    city = value;
}

QString CityWeatherService::getCountry() const
{
    return country;
}

void CityWeatherService::setCountry(const QString &value)
{
    country = value;
}

QString CityWeatherService::getAddressHTTP() const
{
    return addressHTTP;
}

void CityWeatherService::setAddressHTTP(const QString &value)
{
    addressHTTP = value;
}

QString CityWeatherService::getAppId() const
{
    return appId;
}

void CityWeatherService::setAppId(const QString &value)
{
    appId = value;
}

void CityWeatherService::onTimerCityWeather_Tick()
{
    if (readWeather->ReadyToRead())
    {
        currentTimeToRead++;
        if (currentTimeToRead >= timeToRead)
        {
           ForceRead();
        }
    }
}

QString CityWeatherService::getLang() const
{
    return lang;
}

void CityWeatherService::setLang(const QString &value)
{
    lang = value;
}

int CityWeatherService::getTimeToRead() const
{
    return timeToRead;
}

void CityWeatherService::setTimeToRead(int value)
{
    timeToRead = value;
}

void CityWeatherService::ForceRead()
{
    currentTimeToRead = 0;
    auto tempCW = readWeather->Read(getCity() + "," + getCountry(), getAddressHTTP(), getAppId(), getLang());
    if (cityWeather != nullptr)
    {
        delete cityWeather;
        cityWeather = nullptr;
    }
    if (!tempCW->getName().isEmpty())
    {
        cityWeather = tempCW;
    }
}

CityWeather *CityWeatherService::getCityWeather() const
{
    return cityWeather;
}

QString CityWeatherService::remainingTimeToRead()
{
    int restTime = (timeToRead - currentTimeToRead);
    if (restTime < 0)
    {
        restTime = 0;
    }
    int minutes = restTime / 60;
    int seconds = restTime - (minutes * 60);
    return Utils::getNumberWithDigits(minutes, 2) + ":" + Utils::getNumberWithDigits(seconds, 2);
}

