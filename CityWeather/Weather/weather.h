#ifndef WEATHER_H
#define WEATHER_H

#include <QList>
#include "WeatherObject/weatherobject.h"

class Weather
{
public:
    Weather();
    ~Weather();

    QList<WeatherObject *> getWeatherObjects() const;
    void setWeatherObjects(const QList<WeatherObject *> &value);

    void AddWeatherObject(int, QString, QString, QString);

private:
    QList<WeatherObject*> weatherObjects;
};

#endif // WEATHER_H
