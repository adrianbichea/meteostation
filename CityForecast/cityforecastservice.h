#ifndef CITYFORECASTSERVICE_H
#define CITYFORECASTSERVICE_H

#include <QObject>
#include <QTimer>
#include <QString>
#include "forecastread.h"

class CityForecastService : public QObject
{
public:
    CityForecastService(QString city, QString country, QString address, QString appId, int timeToRead, QString lang);
    ~CityForecastService();

    CityForecast *getForecast();

    QString remainingTimeToRead();

    QString getCity() const;
    void setCity(const QString &value);

    QString getAddress() const;
    void setAddress(const QString &value);

    QString getAppId() const;
    void setAppId(const QString &value);

    int getTimeToRead() const;
    void setTimeToRead(int value);

    void ForceRead();

    QString getLang() const;
    void setLang(const QString &value);

private slots:
    void onTimerForecast_Tick();

private:
    ForecastRead* forecastRead;
    CityForecast* forecast;

    QTimer *timerForecast;

    int timeToRead;
    int currentTimeToRead;

    QString city;
    QString address;
    QString appId;

    QString lang;
};

#endif // CITYFORECASTSERVICE_H
