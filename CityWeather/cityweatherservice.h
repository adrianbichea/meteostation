#ifndef CITYWEATHERSERVICE_H
#define CITYWEATHERSERVICE_H

#include <QString>
#include <QTimer>
#include <QObject>

#include "readweather.h"

class CityWeatherService : public QObject
{
public:
    CityWeatherService(QString city, QString country, QString address, QString appId, int timeToRead, QString lang);

    QString getCity() const;
    void setCity(const QString &value);

    QString getCountry() const;
    void setCountry(const QString &value);

    QString getAddressHTTP() const;
    void setAddressHTTP(const QString &value);

    QString getAppId() const;
    void setAppId(const QString &value);

    CityWeather *getCityWeather() const;

    QString remainingTimeToRead();

    int getTimeToRead() const;
    void setTimeToRead(int value);

    void ForceRead();

    QString getLang() const;
    void setLang(const QString &value);

public slots:
    void onTimerCityWeather_Tick();

private:
    QString city;
    QString country;
    QString addressHTTP;
    QString appId;

    int timeToRead;
    int currentTimeToRead;

    QTimer* timerCityWeather;
    ReadWeather* readWeather;

    CityWeather* cityWeather;

    QString lang;
};

#endif // CITYWEATHERSERVICE_H
