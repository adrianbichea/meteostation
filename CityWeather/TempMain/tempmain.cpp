#include "tempmain.h"

TempMain::TempMain(double temp,double feels_like,double temp_min,double temp_max,int pressue,int humidity)
{
    setTemp(temp);
    setFeels_like(feels_like);
    setTemp_min(temp_min);
    setTemp_max(temp_max);
    setPressure(pressue);
    setHumidity(humidity);
}

TempMain::~TempMain()
{

}

double TempMain::getTemp(bool celsius) const
{
    return KelvinToMyUnit(temp, celsius);
}

void TempMain::setTemp(double value)
{
    temp = value;
}

double TempMain::getFeels_like(bool celsius) const
{
    return KelvinToMyUnit(feels_like, celsius);
}

void TempMain::setFeels_like(double value)
{
    feels_like = value;
}

double TempMain::getTemp_min(bool celsius) const
{
    return KelvinToMyUnit(temp_min, celsius);
}

void TempMain::setTemp_min(double value)
{
    temp_min = value;
}

double TempMain::getTemp_max(bool celsius) const
{
    return KelvinToMyUnit(temp_max, celsius);
}

void TempMain::setTemp_max(double value)
{
    temp_max = value;
}

int TempMain::getPressure() const
{
    return pressure;
}

void TempMain::setPressure(int value)
{
    pressure = value;
}

int TempMain::getHumidity() const
{
    return humidity;
}

void TempMain::setHumidity(int value)
{
    humidity = value;
}

double TempMain::KelvinToMyUnit(double value, bool celsius) const
{
    return celsius ? value - 273.5 : (value * 1.8) - 459.67;
}
