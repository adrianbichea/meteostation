#ifndef FORECASTCLOUDS_H
#define FORECASTCLOUDS_H


class ForecastClouds
{
public:
    ForecastClouds();

    int getAll() const;
    void setAll(int value);

private:
    int all;
};

#endif // FORECASTCLOUDS_H
