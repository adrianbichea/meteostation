#ifndef WEATHEROBJECT_H
#define WEATHEROBJECT_H

#include <QString>

class WeatherObject
{
public:
    WeatherObject(int, QString, QString, QString);
    ~WeatherObject() {}

    int getId() const;
    void setId(int value);

    QString getWoMain() const;
    void setWoMain(const QString &value);

    QString getDescription() const;
    void setDescription(const QString &value);

    QString getIcon() const;
    void setIcon(const QString &value);

private:
    int id;
    QString woMain;
    QString description;
    QString icon;
};

#endif // WEATHEROBJECT_H
