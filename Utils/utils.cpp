#include "utils.h"

#include <math.h>

Utils::Utils()
{

}

QString Utils::getNumberWithDigits(int number, int digits)
{
    if (number > digits * 10)
    {
        return QString::number(number);
    }

    QString nr = "";
    for (int i(digits); i > 1; i--)
    {
        if (number < pow(10, i - 1))
        {
            nr += "0";
        }
    }
    return nr + QString::number(number);
}

double Utils::KelvinTo(double value, bool celsius)
{
    return celsius ? value - 273.5 : (value * 1.8) - 459.67;
}

QString Utils::dayOfTheWeekToString(int value)
{
    switch (value)
    {
    case 1:
        return QObject::tr("Monday");
        break;
    case 2:
        return QObject::tr("Tuesday");
        break;
    case 3:
        return QObject::tr("Wednesday");
        break;
    case 4:
        return QObject::tr("Thursday");
        break;
    case 5:
        return QObject::tr("Friday");
        break;
    case 6:
        return QObject::tr("Saturday");
        break;
    case 7:
        return QObject::tr("Sunday");
        break;
    default:
        break;
    }
    return "";
}

QString Utils::getLanguage(eLanguage lang)
{
    if (lang == eLanguage::EN)
    {
        return "en";
    }
    else
    {
        return "fr";
    }
}


