#include "forecastobject.h"

ForecastObject::ForecastObject()
{
    forecastMain = new ForecastMain();
    forecastClouds = new ForecastClouds();
    wind = new Wind(0, 0);
    sys = new ForecastSys();
    weatherObjects = new QList<WeatherObject*>();
}

ForecastObject::~ForecastObject()
{
    delete forecastMain;
    for (auto wo : *weatherObjects)
    {
        delete wo;
    }
    delete forecastClouds;
    delete wind;
    delete sys;
    delete weatherObjects;
}

long ForecastObject::getDt() const
{
    return dt;
}

void ForecastObject::setDt(long value)
{
    dt = value;
}

ForecastMain *ForecastObject::getForecastMain() const
{
    return forecastMain;
}

void ForecastObject::setForecastMain(ForecastMain *value)
{
    forecastMain = value;
}

QList<WeatherObject *> *ForecastObject::getWeatherObjects() const
{
    return weatherObjects;
}

ForecastClouds *ForecastObject::getForecastClouds() const
{
    return forecastClouds;
}

void ForecastObject::setForecastClouds(ForecastClouds *value)
{
    forecastClouds = value;
}

Wind *ForecastObject::getWind() const
{
    return wind;
}

void ForecastObject::setWind(Wind *value)
{
    wind = value;
}

int ForecastObject::getVisibility() const
{
    return visibility;
}

void ForecastObject::setVisibility(int value)
{
    visibility = value;
}

double ForecastObject::getPop() const
{
    return pop;
}

void ForecastObject::setPop(double value)
{
    pop = value;
}

ForecastSys *ForecastObject::getSys() const
{
    return sys;
}

void ForecastObject::setSys(ForecastSys *value)
{
    sys = value;
}

QString ForecastObject::getDt_txt() const
{
    return dt_txt;
}

void ForecastObject::setDt_txt(const QString &value)
{
    dt_txt = value;
}
