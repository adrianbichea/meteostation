#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QHostAddress>
#include <QFile>

#include "Forms/Preferences/preferencesdialog.h"
#include "Forms/Forecast/forecastdialog.h"
#include "Utils/utils.h"

MainWindow::MainWindow(QApplication *myApp, QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    myApplication = myApp;

    ui->setupUi(this);

    ChangeLanguage(p.getLanguage());

    //language bug
    ui->actionPreferences->setText(tr("Preferences"));
    ui->actionView_Forecast->setText(tr("View Forecast"));
    ui->tabMeteo->setTabText(1, tr("Sea weather"));

    bDayNight = true;

    ui->lblError->setVisible(false);

    sws = new SeaWeatherService(p.getUpdateBME280Time(), p.getIpSea(), p.getPortSea(), p.getCommunicationType() == TCPIP);

    cws = new CityWeatherService(p.getCity(), p.getCountry(), p.getAddressHTTP(), p.getAppId(), p.getUpdateHTTPTime(),
                                 Utils::getLanguage(p.getLanguage()));
    cfs = new CityForecastService(p.getCity(), p.getCountry(), p.getAddressHTTP(), p.getAppId(), p.getUpdateHTTPTime(),
                                  Utils::getLanguage(p.getLanguage()));

    seaTimer = new QTimer();
    seaTimer->setInterval(1000);
    seaTimer->start();
    connect(seaTimer, SIGNAL(timeout()), this, SLOT(SeaTimer_Tick()));
    RefreshSeaWeather();
    RefreshCityWeather();
    UpdatePreferences();
}

MainWindow::~MainWindow()
{
    delete seaTimer;
    delete ui;
}

void MainWindow::SeaTimer_Tick()
{
    RefreshSeaWeather();
    RefreshCityWeather();
}


void MainWindow::on_actionPreferences_triggered()
{
    PreferencesDialog prefDialog(this);
    prefDialog.exec();
    UpdatePreferences();
    qDebug() << "cws->ForceRead();";
    cws->ForceRead();
    cfs->ForceRead();
    RefreshSeaWeather();
    RefreshCityWeather();
}

void MainWindow::UpdatePreferences()
{
    p.LoadConfiguration();

    if (p.getDayMode() != AUTO)
    {
        bDayNight = p.getDayMode() == DAY ? true : false;
    }

    QString filename = bDayNight ? p.getDayStyleSheet() : p.getNightStyleSheet();
    if (!QFile::exists(filename))
    {
        filename = ":/combobox/default.qcc";
    }
    QFile fileCSS(filename);

    bool openOK = fileCSS.open(QIODevice::ReadOnly);
    if (openOK)
    {
        QString infosCSS = "";
        infosCSS += "QLabel {\n";
        auto f = bDayNight ? p.getDayFont() : p.getNightFont();
        infosCSS += "font-family: " + f.getFontName() + ";\n";
        infosCSS += "font-size: " + QString::number(f.getSize()) + "pt;\n";
        if (f.getBold())
        {
            infosCSS += "font-weight: bold;\n";
        }
        if (f.getItalic())
        {
            infosCSS += "font-style: italic;\n";
        }
        if (f.getUnderline())
        {
            infosCSS += "text-decoration: underline;\n";
        }
        infosCSS += "}\n";

        infosCSS += fileCSS.readAll();

        setStyleSheet(infosCSS);
        fileCSS.close();
    }
    else
    {
        qDebug() << "Error loading styles!";
    }
    ChangeLanguage(p.getLanguage());
    sws->SetNewTimeRefresh(p.getUpdateBME280Time());
    sws->setIsTCPConnection(p.getCommunicationType() == TCPIP);
    sws->getSeaWeather()->setIpConnection(p.getIpSea());
    sws->getSeaWeather()->setPortConnection(p.getPortSea());

    cws->setTimeToRead(p.getUpdateHTTPTime());
    cws->setCity(p.getCity());
    cws->setCountry(p.getCountry());
    cws->setAddressHTTP(p.getAddressHTTP());
    cws->setAppId(p.getAppId());
    cws->setLang(Utils::getLanguage(p.getLanguage()));

    cfs->setTimeToRead(p.getUpdateHTTPTime());
    cfs->setCity(p.getCity() + "," + p.getCountry());
    cfs->setAddress(p.getAddressHTTP());
    cfs->setAppId(p.getAppId());
    cfs->setLang(Utils::getLanguage(p.getLanguage()));
}

void MainWindow::RefreshSeaWeather()
{
    ui->lblSensorName->setText(tr("Sensor name: ") + sws->getSeaWeather()->getSensorName());
    ui->lblHumidity->setText(tr("Humidity: ") + QString::number(sws->getSeaWeather()->getHumidity(), 'f', 1) + " %");
    ui->lblPressure->setText(tr("Pressure: ") + QString::number(sws->getSeaWeather()->getPressure(), 'f', 1) + tr(" mbar"));
    QString celsiusOrFahr = (p.getTypeTemperature() == FAHRENHEIT) ? " F" : " C";
    ui->lblTemperature->setText(tr("Temperature: ") +
                                QString::number(sws->getSeaWeather()->getTemperature(p.getTypeTemperature() == CELSIUS), 'f', 1) +
                                celsiusOrFahr);
    if (p.getTypeTemperature() == FAHRENHEIT)
    {
        ui->imgSeaTypeTemp->setPixmap(QPixmap(":/Meteo/fahr_term"));
    }
    else
    {
        ui->imgSeaTypeTemp->setPixmap(QPixmap(":/Meteo/celsius_therm"));
    }
    ui->lblAltitude->setText(tr("Altitude: ") + QString::number(sws->getSeaWeather()->getAltitude(), 'f', 1) + " m.");
    QDateTime timestamp;
    timestamp.setTime_t(sws->getSeaWeather()->getTimestamp());
    if (p.getHourFormat() == H24)
    {
        ui->lblTimeStampSea->setText(tr("Local time: ") + timestamp.toString("dd-MM-yyyy hh:mm:ss"));
    }
    else
    {
        ui->lblTimeStampSea->setText(tr("Local time: ") + timestamp.toString("dd-MM-yyyy h:mm:ss ap"));
    }
    ui->lblNextReadSea->setText(tr("Time until next update: ") + sws->getTimeUntilUpdate());

    ui->lblError->setVisible(false);
    if (sws->getSeaWeather()->getLastConnectionTimeOut())
    {
        ui->lblError->setVisible(true);
    }
    ui->lblError->setText(tr("Connection timeout !"));
}

void MainWindow::RefreshCityWeather()
{
    bool isCelsius = p.getTypeTemperature() == CELSIUS;
    ui->tabMeteo->setTabText(0, tr("City not found!"));
    if (cws->getCityWeather() != nullptr)
    {
        //qDebug() << "Dt: " << cws->getCityWeather()->getDt();
        QDateTime timestamp;
        timestamp.setTime_t(cws->getCityWeather()->getDt());
        timestamp = timestamp.addSecs(cws->getCityWeather()->getTimezone() - timestamp.offsetFromUtc());
        if (p.getHourFormat() == H24)
        {
            ui->lblLocTime->setText(tr("Last reading: ") + timestamp.toString("dd-MM-yyyy hh:mm:ss"));
        }
        else
        {
            ui->lblLocTime->setText(tr("Last reading: ") + timestamp.toString("dd-MM-yyyy h:mm:ss ap"));
        }

        if (cws->getCityWeather()->getName().length() < 20)
        {
            ui->lblCityName->setText(cws->getCityWeather()->getName() + ", " + cws->getCountry());
            ui->tabMeteo->setTabText(0, cws->getCityWeather()->getName());
        }
        else
        {
            ui->lblCityName->setText(p.getCity() + ", " + cws->getCountry());
            ui->tabMeteo->setTabText(0, p.getCity());
        }
        ui->lblTemp->setText(QString::number(cws->getCityWeather()->getWmain()->getTemp(isCelsius), 'f', 1));

        QString szCelsius = " °C";
        if (p.getTypeTemperature() == FAHRENHEIT)
        {
            ui->degType->setPixmap(QPixmap(":/Meteo/fahrenheit"));
            szCelsius = " °F";
        }
        else
        {
            ui->degType->setPixmap(QPixmap(":/Meteo/celsius"));
        }

        QString szIconWeather = ":/Meteo/01d";
        QString szWeatherType = tr("Unknown");
        if (cws->getCityWeather()->getWeather()->getWeatherObjects().count() != 0)
        {
            szIconWeather = ":/Meteo/" + cws->getCityWeather()->getWeather()->getWeatherObjects().at(0)->getIcon();
            szWeatherType = cws->getCityWeather()->getWeather()->getWeatherObjects().at(0)->getDescription();
        }
        if (p.getDayMode() == AUTO)
        {
            QString dayNight = "" + szIconWeather[szIconWeather.length() - 1];
            bool b = dayNight == "d" ? true : false;
            if (b != bDayNight)
            {
                bDayNight = b;
                UpdatePreferences();
            }
        }

        ui->lblCityFeelsLike->setText(tr("Feels like: ") + QString::number(cws->getCityWeather()->getWmain()->getFeels_like(isCelsius), 'f', 1) + szCelsius);
        ui->lblCityMinTemp->setText(tr("Min: ") + QString::number(cws->getCityWeather()->getWmain()->getTemp_min(isCelsius), 'f', 1) + szCelsius);
        ui->lblCityMaxTemp->setText(tr("Max: ") + QString::number(cws->getCityWeather()->getWmain()->getTemp_max(isCelsius), 'f', 1) + szCelsius);
        ui->lblCityHumidity->setText(tr("Humidity: ") + QString::number(cws->getCityWeather()->getWmain()->getHumidity()) + " %");
        ui->lblCityPressure->setText(tr("Pressure: ") + QString::number(cws->getCityWeather()->getWmain()->getPressure()) + tr(" mbar"));
        ui->lblCityWindSpeed->setText(tr("Wind speed: ") + QString::number(cws->getCityWeather()->getWind()->getSpeed()) + tr(" m/sec"));

        QPixmap pixmap(":/Meteo/arrow");
        QMatrix rm;
        rm.rotate(180 + cws->getCityWeather()->getWind()->getDeg());
        pixmap = pixmap.transformed(rm);
        ui->lblWindAngle->setPixmap(pixmap);

        ui->iconWeather->setPixmap(QPixmap(szIconWeather));

        ui->lblCityWeatherType->setText(tr("Weather type: ") + szWeatherType);
    }
    else
    {
        //qDebug() << "NULLPTR";
        ClearCityWeather();
        ui->lblCityName->setText(cws->getCity() + ", " + cws->getCountry() + tr(" - doesnt exist!"));
        ui->lblCityFeelsLike->setText("---");
        ui->lblCityMinTemp->setText("---");
        ui->lblCityMaxTemp->setText("---");
        ui->lblCityHumidity->setText("---");
        ui->lblCityPressure->setText("---");
        ui->lblCityWindSpeed->setText("---");
        ui->lblCityWeatherType->setText("---");
        ui->lblLocTime->setText(tr("Last reading: ") + "---");
    }

    ui->iconDay1->setToolTip("");
    ui->iconDay2->setToolTip("");
    ui->iconDay3->setToolTip("");
    ui->iconDay4->setToolTip("");
    ui->iconDay5->setToolTip("");

    if (cfs->getForecast())
    {
        int tz = cfs->getForecast()->getCity()->getTimezone();
        ui->lblDay1->setText(cfs->getForecast()->getDayOfTheWeekAndDayOfMonth(1, tz));
        ui->lblDay2->setText(cfs->getForecast()->getDayOfTheWeekAndDayOfMonth(2, tz));
        ui->lblDay3->setText(cfs->getForecast()->getDayOfTheWeekAndDayOfMonth(3, tz));
        ui->lblDay4->setText(cfs->getForecast()->getDayOfTheWeekAndDayOfMonth(4, tz));
        ui->lblDay5->setText(cfs->getForecast()->getDayOfTheWeekAndDayOfMonth(5, tz));
        ui->tempDay1->setText(cfs->getForecast()->getForecastTemperature(1, isCelsius, tz));
        ui->tempDay2->setText(cfs->getForecast()->getForecastTemperature(2, isCelsius, tz));
        ui->tempDay3->setText(cfs->getForecast()->getForecastTemperature(3, isCelsius, tz));
        ui->tempDay4->setText(cfs->getForecast()->getForecastTemperature(4, isCelsius, tz));
        ui->tempDay5->setText(cfs->getForecast()->getForecastTemperature(5, isCelsius, tz));
        ui->lblDescription1->setText(cfs->getForecast()->getDescription(1, tz));
        ui->lblDescription2->setText(cfs->getForecast()->getDescription(2, tz));
        ui->lblDescription3->setText(cfs->getForecast()->getDescription(3, tz));
        ui->lblDescription4->setText(cfs->getForecast()->getDescription(4, tz));
        ui->lblDescription5->setText(cfs->getForecast()->getDescription(5, tz));

        QString icn = cfs->getForecast()->getIcon(1, tz);
        if (icn != "unknown")
        {
            ui->iconDay1->setPixmap(QPixmap(":/Meteo/" + icn));
        }
        else
        {
            ui->iconDay1->setPixmap(*ui->iconWeather->pixmap());
        }
        ui->iconDay2->setPixmap(QPixmap(":/Meteo/" + cfs->getForecast()->getIcon(2, tz)));
        ui->iconDay3->setPixmap(QPixmap(":/Meteo/" + cfs->getForecast()->getIcon(3, tz)));
        ui->iconDay4->setPixmap(QPixmap(":/Meteo/" + cfs->getForecast()->getIcon(4, tz)));
        ui->iconDay5->setPixmap(QPixmap(":/Meteo/" + cfs->getForecast()->getIcon(5, tz)));

        ui->iconDay1->setToolTip(cfs->getForecast()->getToolTip(1, isCelsius, tz));
        ui->iconDay2->setToolTip(cfs->getForecast()->getToolTip(2, isCelsius, tz));
        ui->iconDay3->setToolTip(cfs->getForecast()->getToolTip(3, isCelsius, tz));
        ui->iconDay4->setToolTip(cfs->getForecast()->getToolTip(4, isCelsius, tz));
        ui->iconDay5->setToolTip(cfs->getForecast()->getToolTip(5, isCelsius, tz));
    }
    else
    {
        ui->lblDay1->setText("---");
        ui->lblDay2->setText("---");
        ui->lblDay3->setText("---");
        ui->lblDay4->setText("---");
        ui->lblDay5->setText("---");
        ui->tempDay1->setText("---");
        ui->tempDay2->setText("---");
        ui->tempDay3->setText("---");
        ui->tempDay4->setText("---");
        ui->tempDay5->setText("---");
        ui->lblDescription1->setText("---");
        ui->lblDescription2->setText("---");
        ui->lblDescription3->setText("---");
        ui->lblDescription4->setText("---");
        ui->lblDescription5->setText("---");
    }
    ui->lblNextUpdateHTTP->setText(tr("Next update in: ") + cws->remainingTimeToRead());
}

void MainWindow::ClearCityWeather()
{
    ui->lblCityName->setText("***");
    ui->lblTemp->setText("---");
}

void MainWindow::ChangeLanguage(eLanguage lang)
{
    if (lang == eLanguage::FR)
    {
        qDebug() << "Changing language to FR";
        translator.load(":/fr_lang/hellotr_fr.qm");
        myApplication->installTranslator(&translator);
    }
    else
    {
        myApplication->removeTranslator(&translator);
    }
    //language bug
    ui->actionPreferences->setText(tr("Preferences"));
    ui->actionView_Forecast->setText(tr("View Forecast"));
    ui->tabMeteo->setTabText(1, tr("Sea weather"));
}

void MainWindow::on_actionView_Forecast_triggered()
{
    ForecastDialog fd;
    fd.ShowChart(cfs->getForecast()->getForecastObjects(), p.getTypeTemperature() == CELSIUS);
    fd.exec();
}
