#ifndef CITYFORECAST_H
#define CITYFORECAST_H

#include <QString>
#include <QList>

#include "forecastobject.h"
#include "cityforecastobject.h"

class CityForecast
{
public:
    CityForecast();
    ~CityForecast();

    QString getCod() const;
    void setCod(const QString &value);

    int getMessage() const;
    void setMessage(int value);

    int getCnt() const;
    void setCnt(int value);

    QList<ForecastObject *> *getForecastObjects() const;

    CityForecastObject *getCity() const;
    void setCity(CityForecastObject *value);

    //day = 1, 2, 3, 4 or 5
    //pop - from object->sys = "d" or "n" - day/night
    QString getForecastTemperature(int day, bool celsius, int timezone);

    //day = 1, 2, 3, 4 or 5
    QString getDayOfTheWeekAndDayOfMonth(int day, int timezone);

    QString getIcon(int day, int timezone);

    QString getDescription(int day, int timezone);

    QString getToolTip(int day, bool celsius, int timezone);

private:

    QString cod;
    int message;
    int cnt;
    QList<ForecastObject*> *forecastObjects;
    CityForecastObject* city;
};

#endif // CITYFORECAST_H
