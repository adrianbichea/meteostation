#include "preferencesdialog.h"
#include "ui_preferencesdialog.h"

#include <QFont>
#include <QFontDialog>
#include <QColor>
#include <QColorDialog>
#include <QFileDialog>
#include <QDir>

#include <QPalette>
#include <QFile>

#include <QDebug>
#include "Forms/Styles/styledialog.h"

PreferencesDialog::PreferencesDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PreferencesDialog)
{
    ui->setupUi(this);

    ui->comboCountry->setView(new QListView());
    QFile fss(":/combobox/combobox_style.qss");
    if (fss.exists() && fss.open(QIODevice::ReadOnly))
    {
        auto all = fss.readAll();
        ui->comboCountry->setStyleSheet(all);
        fss.close();
    }

    countriesMap.PopulateMap(":/countries/countries");

    auto mCountries = countriesMap.getMap();
    for (auto it = mCountries.begin(); it != mCountries.end(); it++)
    {
        ui->comboCountry->addItem(it.value());
    }

    ui->radio12h->setChecked(p.getHourFormat() == H12);
    ui->radio24h->setChecked(p.getHourFormat() == H24);

    ui->radioF->setChecked(p.getTypeTemperature() == FAHRENHEIT);
    ui->radioC->setChecked(p.getTypeTemperature() == CELSIUS);

    ui->radioEN->setChecked(p.getLanguage() == EN);
    ui->radioFR->setChecked(p.getLanguage() == FR);

    ui->updateValueBME280->setValue(p.getUpdateBME280Time());
    ui->updateValueHTTP->setValue(p.getUpdateHTTPTime());

    ui->radioDay->setChecked(p.getDayMode() == DAY);
    ui->radioNight->setChecked(p.getDayMode() == NIGHT);
    ui->radioAuto->setChecked(p.getDayMode() == AUTO);

    ui->lblDayText->setFont(p.getDayFont().getFont());
    ui->lblNightText->setFont(p.getNightFont().getFont());

    ui->txtCity->setText(p.getCity());
    ui->comboCountry->setCurrentText(countriesMap.getValueFromKey(p.getCountry()));
    ui->txtAddress->setText(p.getAddressHTTP());

    ui->txtIP->setText(p.getIpSea());
    ui->txtPort->setText(QString::number(p.getPortSea()));

    QIcon iconGeneral(":/Meteo/general");
    QListWidgetItem *itemGeneral = new QListWidgetItem(iconGeneral, tr("General options"));
    ui->listOptions->addItem(itemGeneral);

    QIcon iconFont(":/Meteo/font");
    QListWidgetItem *itemFont = new QListWidgetItem(iconFont, tr("Font"));
    ui->listOptions->addItem(itemFont);

    QIcon iconBME280(":/Meteo/bme280");
    QListWidgetItem *itemBME280= new QListWidgetItem(iconBME280, tr("BME280"));
    ui->listOptions->addItem(itemBME280);

    QIcon iconHTTP(":/Meteo/http");
    QListWidgetItem *itemHTTP= new QListWidgetItem(iconHTTP, tr("HTTP/HTTPS"));
    ui->listOptions->addItem(itemHTTP);

    ui->listOptions->setCurrentRow(0);
    ui->groupBME280->setVisible(false);
    ui->groupFont->setVisible(false);
    ui->groupHTTP_HTTPS->setVisible(false);
    ui->groupGeneralOptions->setVisible(true);


    ui->txtAppId->setText(p.getAppId());

    ui->lblDayText->setStyleSheet(p.getDayFont().getStyleSheet());
    ui->lblNightText->setStyleSheet(p.getNightFont().getStyleSheet());

    UpdateStyleSheet();
}

PreferencesDialog::~PreferencesDialog()
{
    delete ui;
}

void PreferencesDialog::on_btnDayFont_clicked()
{
    bool ok;
    QFont font = QFontDialog::getFont(&ok, ui->lblDayText->font(), this);
    if (ok)
    {
        FontPreferences fp(font.family(), font.bold(), font.italic(), font.underline(), font.pointSize());
        p.setDayFont(fp);
        ui->lblDayText->setStyleSheet(p.getDayFont().getStyleSheet());
        ui->lblNightText->setStyleSheet(p.getNightFont().getStyleSheet());
    }
}

void PreferencesDialog::on_btnSDayStyle_clicked()
{
    auto fileName = QFileDialog::getOpenFileName(this,
        tr("Open Stylesheet"), QDir::currentPath(), tr("Stylesheet files (*.qcc *.css)"));

    if (!fileName.isEmpty())
    {
        p.setDayStyleSheet(fileName);
    }
}

void PreferencesDialog::on_btnNightStyle_clicked()
{
    auto fileName = QFileDialog::getOpenFileName(this,
        tr("Open Stylesheet"), QDir::currentPath(), tr("Stylesheet files (*.qcc *.css)"));

    if (!fileName.isEmpty())
    {
        p.setNightStyleSheet(fileName);
    }
}

void PreferencesDialog::on_btnNightFont_clicked()
{
    bool ok;
    QFont font = QFontDialog::getFont(&ok, ui->lblNightText->font(), this);
    if (ok)
    {
        FontPreferences fp(font.family(), font.bold(), font.italic(), font.underline(), font.pointSize());
        p.setNightFont(fp);
        ui->lblDayText->setStyleSheet(p.getDayFont().getStyleSheet());
        ui->lblNightText->setStyleSheet(p.getNightFont().getStyleSheet());
    }
}

void PreferencesDialog::on_listOptions_itemClicked(QListWidgetItem *item)
{
    qDebug() << item->text();
    ui->groupGeneralOptions->setVisible(false);
    ui->groupBME280->setVisible(false);
    ui->groupFont->setVisible(false);
    ui->groupHTTP_HTTPS->setVisible(false);
    if (item->text().contains(tr("General options")))
    {
        ui->groupGeneralOptions->setVisible(true);
    }
    if (item->text().contains(tr("Font")))
    {
        ui->groupFont->setVisible(true);
    }
    if (item->text().contains(tr("BME280")))
    {
        ui->groupBME280->setVisible(true);
    }
    if (item->text().contains(tr("HTTP/HTTPS")))
    {
        ui->groupHTTP_HTTPS->setVisible(true);
    }
    UpdateStyleSheet();
}

void PreferencesDialog::closeEvent(QCloseEvent *bar)
{
    p.setHourFormat(ui->radio12h->isChecked() ? H12 : H24);
    p.setTypeTemperature(ui->radioF->isChecked() ? FAHRENHEIT : CELSIUS);
    p.setLanguage(ui->radioEN->isChecked() ? EN : FR);
    p.setCommunicationType(TCPIP);
    p.setDayMode(ui->radioDay->isChecked() ? DAY : ui->radioNight->isChecked() ? NIGHT : AUTO);
    p.setUpdateBME280Time(ui->updateValueBME280->value());
    p.setUpdateHTTPTime(ui->updateValueHTTP->value());

    p.setCity(ui->txtCity->text());
    p.setCountry(countriesMap.getAlpha2(ui->comboCountry->currentText()));
    qDebug() << ui->comboCountry->currentText();
    qDebug() << countriesMap.getAlpha2(ui->comboCountry->currentText());
    p.setAddressHTTP(ui->txtAddress->text());

    p.setIpSea(ui->txtIP->text());
    p.setPortSea(ui->txtPort->text().toInt());

    p.setAppId(ui->txtAppId->text());

    p.SaveConfiguration();

    bar->accept();
}

void PreferencesDialog::OpenStyleDialog(QString filename)
{
    QFile f(filename);
    if (f.open(QIODevice::ReadOnly))
    {
        QString searchColor1 = "stop:0 ";
        QString searchColor2 = "stop:1 ";
        QString searchText = "color: rgb(";
        QString styleStr = f.readAll();
        int colorIndex1 = styleStr.indexOf(searchColor1);
        int colorIndex2 = styleStr.indexOf(searchColor2);
        int textColorIndex = styleStr.indexOf(searchText);
        //qDebug() << colorIndex1 << " " << colorIndex2 << " " << textColorIndex;
        QString color1 = styleStr.mid(colorIndex1 + searchColor1.length(), 7);
        QString color2 = styleStr.mid(colorIndex2 + searchColor2.length(), 7);
        QString textColor = styleStr.mid(textColorIndex + searchText.length(), 16);
        auto allColors = textColor.split(')');
        auto colors = allColors[0].split(',');
        QColor textC = QColor::fromRgb(colors[0].toInt(), colors[1].toInt(), colors[2].toInt());

        qDebug() << color1 << " " << color2 << " " << textC.red() << " " << textC.green() << " " << textC.blue();
        StyleDialog* sd = new StyleDialog(QColor(color1), textC, QColor(color2));
        sd->setFilename(filename);
        sd->exec();
        delete sd;
        UpdateStyleSheet();
    }
}

void PreferencesDialog::UpdateStyleSheet()
{
    QFile daySheet(GetFilename(p.getDayStyleSheet()));
    if (daySheet.open(QIODevice::ReadOnly))
    {
        auto fText = daySheet.readAll();
        fText.replace("#frameSea, #frameCity", "");
        ui->frameDay->setStyleSheet(fText);
        daySheet.close();
        qDebug().noquote() << fText;
    }
    QFile nightSheet(GetFilename(p.getNightStyleSheet()));
    if (nightSheet.open(QIODevice::ReadOnly))
    {
        auto fText = nightSheet.readAll();
        fText.replace("#frameSea, #frameCity", "");
        ui->frameNight->setStyleSheet(fText);
        qDebug().noquote() << fText;
        nightSheet.close();
    }
}

QString PreferencesDialog::GetFilename(QString filenameStyleSheet)
{
    QString filename = filenameStyleSheet;
    if (!QFile::exists(filename))
    {
        filename = "defaultNight.qss";
        QFile::copy(":/combobox/default.qcc", filename);
        p.setNightStyleSheet(filename);
    }
    return filename;
}

void PreferencesDialog::on_btnCreateStyleNight_clicked()
{

    OpenStyleDialog(GetFilename(p.getNightStyleSheet()));
}

void PreferencesDialog::on_btnCreateStyleDay_clicked()
{
    OpenStyleDialog(GetFilename(p.getDayStyleSheet()));
}
