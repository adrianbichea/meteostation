#include "tcp.h"
#include "bme280.h"

void start_server()
{
	int listenfd = 0, connfd = 0;
	struct sockaddr_in serv_addr;

	char sendBuff[1025];

	listenfd = socket(AF_INET, SOCK_STREAM, 0);
	if (listenfd == -1)
	{
		printf("Could not create socket!");
		exit(-1);
	}

	memset(&serv_addr, '0', sizeof(serv_addr));
	memset(sendBuff, '0', sizeof(sendBuff));

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(12002);

	if (bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) != 0)
	{
		printf("Bind failed!\n");
		exit(-2);
	}

	if (listen(listenfd, 10) != 0)
	{
		printf("Listen failed!\n");
		exit(-3);
	}

	printf("Server TCP - STARTED\n");

	while(1)
	{
		printf("Waiting connection...");
        	connfd = accept(listenfd, (struct sockaddr*)NULL, NULL);
		printf("\nConnection accepted!\nSending file...");

		read_bme280();
		//reading file and sending it
		FILE* f = fopen("bme280_data.txt", "r");
		if (f == 0)
		{
			printf("Failed to open file bme280_data.txt\n");
		}
		else
		{
			ssize_t read;
			char* line = NULL;
			size_t len = 0;
			while ((read = getline(&line, &len, f)) != -1)
			{
				snprintf(sendBuff, sizeof(sendBuff), "%s", line);
				printf("\nSending: %s", line);
		        	write(connfd, sendBuff, strlen(sendBuff));
			}
			fclose(f);
			if (line)
			{
				free(line);
			}
			printf("\nFile sent.\n");
		}
		close(connfd);
		printf("End connection!\n\n");
		sleep(1);
	}
}
