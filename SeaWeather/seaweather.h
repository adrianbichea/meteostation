#ifndef SEAWEATHER_H
#define SEAWEATHER_H

#include <QString>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QDebug>
#include <QTcpSocket>
#include <QObject>
#include <QTimer>

class SeaWeather : public QObject
{
public:
    SeaWeather(QString, int);
    ~SeaWeather();

    QString getSensorName() const;
    void setSensorName(const QString &value);

    double getHumidity() const;
    void setHumidity(double value);

    double getPressure() const;
    void setPressure(double value);

    double getTemperature(bool typeTemp) const;
    void setTemperature(double value);

    double getAltitude() const;
    void setAltitude(double value);

    long getTimestamp() const;
    void setTimestamp(double value);

    void ReadWeather(bool);

    bool getUpdating() const;

    bool getLastConnectionTimeOut() const;

    QString getIpConnection() const;
    void setIpConnection(const QString &value);

    int getPortConnection() const;
    void setPortConnection(int value);

public slots:
    void onReadyRead();
    void onTimeOutReached();

private:
    QString sensorName;
    double humidity;
    double pressure;
    double temperature;
    double altitude;
    long timestamp;

    QTcpSocket socket;

    bool updating;
    const int timeoutTCP = 10;
    int countTimer;
    QTimer* timer;

    QString ipConnection;
    int portConnection;

    bool lastConnectionTimeOut;

    void UpdateData(QByteArray);


};

#endif // SEAWEATHER_H
