#include "cityweather.h"

#include <QList>

CityWeather::CityWeather()
{

    setId(0);
    setName("");
    coord = new Coord(0, 0);
    wmain=new TempMain(0,0,0,0,0,0);
    setDt(0);
    wind=new Wind(0,0);
    sys=new SysCountry("0");
    setRain(0);
    clouds=new Clouds(0);
    weather = new Weather();
    setBase("");
    visibility = 0;
    setTimezone(0);
    setCod(0);
}

CityWeather::~CityWeather()
{
    if (coord != nullptr)
    {
        delete coord;
    }
    if (weather != nullptr)
    {
        delete weather;
    }
    if (wmain != nullptr)
    {
        delete wmain;
    }
    if (sys != nullptr)
    {
        delete sys;
    }
    if (clouds != nullptr)
    {
        delete clouds;
    }
    if (wind != nullptr)
    {
        delete wind;
    }
}

int CityWeather::getId() const
{
    return id;
}

void CityWeather::setId(int value)
{
    id = value;
}

QString CityWeather::getName() const
{
    return name;
}

void CityWeather::setName(const QString &value)
{
    name = value;
}

Coord *CityWeather::getCoord() const
{
    return coord;
}

void CityWeather::setCoord(Coord *value)
{
    coord = value;
}

Weather *CityWeather::getWeather() const
{
    return weather;
}

void CityWeather::setWeather(Weather *value)
{
    weather = value;
}

TempMain *CityWeather::getWmain() const
{
    return wmain;
}

void CityWeather::setWmain(TempMain *value)
{
    wmain = value;
}

long CityWeather::getDt() const
{
    return dt;
}

void CityWeather::setDt(long value)
{
    dt = value;
}

Wind *CityWeather::getWind() const
{
    return wind;
}

void CityWeather::setWind(Wind *value)
{
    wind = value;
}

SysCountry *CityWeather::getSys() const
{
    return sys;
}

void CityWeather::setSys(SysCountry *value)
{
    sys = value;
}

double CityWeather::getRain() const
{
    return rain;
}

void CityWeather::setRain(double value)
{
    rain = value;
}

Clouds *CityWeather::getClouds() const
{
    return clouds;
}

void CityWeather::setClouds(Clouds *value)
{
    clouds = value;
}

QString CityWeather::getBase() const
{
    return _base;
}

void CityWeather::setBase(const QString &base)
{
    _base = base;
}

int CityWeather::getVisibility() const
{
    return visibility;
}

void CityWeather::setVisibility(int value)
{
    visibility = value;
}

int CityWeather::getTimezone() const
{
    return timezone;
}

void CityWeather::setTimezone(int value)
{
    timezone = value;
}

int CityWeather::getCod() const
{
    return cod;
}

void CityWeather::setCod(int value)
{
    cod = value;
}

