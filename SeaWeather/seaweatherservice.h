#ifndef SEAWEATHERSERVICE_H
#define SEAWEATHERSERVICE_H

#include <QTimer>
#include <QObject>
#include <QString>


#include "SeaWeather/seaweather.h"

class SeaWeatherService : public QObject
{
public:
    SeaWeatherService(int timeRefresh, QString ip, int port, bool isTCP);
    ~SeaWeatherService();

    QString getTimeUntilUpdate();
    void SetNewTimeRefresh(int);

    SeaWeather *getSeaWeather() const;

    bool getIsTCPConnection() const;
    void setIsTCPConnection(bool value);

private slots:
    void Timer_Tick();

private:
    QTimer* timer;

    int timeOutRefresh;
    int currentTimeOut;

    SeaWeather* seaWeather;
    bool isTCPConnection;

    void ReadData();
};

#endif // SEAWEATHERSERVICE_H
