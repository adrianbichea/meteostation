#include <QFile>

#include "preferences.h"

Preferences::Preferences()
{
    setHourFormat(eHourFormat::H24);
    setTypeTemperature(eTypeTemperature::CELSIUS);
    setCity("");
    setCountry("");
    setLanguage(eLanguage::EN);

    setCommunicationType(eTypeCommunicationBME280::TCPIP);
    setUpdateBME280Time(300);
    setUpdateHTTPTime(300);

    LoadConfiguration();
}

Preferences::~Preferences()
{

}

eHourFormat Preferences::getHourFormat() const
{
    return hourFormat;
}

void Preferences::setHourFormat(const eHourFormat &value)
{
    hourFormat = value;
}

eTypeTemperature Preferences::getTypeTemperature() const
{
    return typeTemperature;
}

void Preferences::setTypeTemperature(const eTypeTemperature &value)
{
    typeTemperature = value;
}

QString Preferences::getCity() const
{
    return city;
}

void Preferences::setCity(const QString &value)
{
    city = value;
}

QString Preferences::getCountry() const
{
    return country;
}

void Preferences::setCountry(const QString &value)
{
    country = value;
}

FontPreferences Preferences::getDayFont() const
{
    return dayFont;
}

void Preferences::setDayFont(const FontPreferences &value)
{
    dayFont = value;
}

FontPreferences Preferences::getNightFont() const
{
    return nightFont;
}

void Preferences::setNightFont(const FontPreferences &value)
{
    nightFont = value;
}

eLanguage Preferences::getLanguage() const
{
    return language;
}

void Preferences::setLanguage(const eLanguage &value)
{
    language = value;
}

eTypeCommunicationBME280 Preferences::getCommunicationType() const
{
    return communicationType;
}

void Preferences::setCommunicationType(const eTypeCommunicationBME280 &value)
{
    communicationType = value;
}

int Preferences::getUpdateBME280Time() const
{
    return updateBME280Time;
}

void Preferences::setUpdateBME280Time(int value)
{
    updateBME280Time = value;
}

int Preferences::getUpdateHTTPTime() const
{
    return updateHTTPTime;
}

void Preferences::setUpdateHTTPTime(int value)
{
    updateHTTPTime = value;
}

void Preferences::SaveConfiguration()
{
    QSettings config(CONFIGURATION_FILENAME, QSettings::IniFormat);

    config.setValue(HOUR_FORMAT, getHourFormat());
    config.setValue(TYPE_TEMPERATURE, getTypeTemperature());
    config.setValue(CITY, getCity());
    config.setValue(COUNTRY, getCountry());
    config.setValue(LANGUAGE, getLanguage());
    config.setValue(COMMUNICATION_TYPE, getCommunicationType());
    config.setValue(UPDATE_BME280, getUpdateBME280Time());
    config.setValue(UPDATE_HTTP, getUpdateHTTPTime());
    config.setValue(DAY_STYLE_SHEET, getDayStyleSheet());
    config.setValue(NIGHT_STYLE_SHEET, getNightStyleSheet());
    config.setValue(DAY_MODE, getDayMode());
    config.setValue(DAY_FONT, getDayFont().toString());
    config.setValue(NIGHT_FONT, getNightFont().toString());
    config.setValue(IP_SEA, getIpSea());
    config.setValue(PORT_SEA, getPortSea());
    config.setValue(ADDRESS_HTTP, getAddressHTTP());
    config.setValue(APP_ID, getAppId());
}

void Preferences::LoadConfiguration()
{
    if (QFile::exists(CONFIGURATION_FILENAME))
    {
        QSettings config(CONFIGURATION_FILENAME, QSettings::IniFormat);

        setHourFormat(static_cast<eHourFormat>(config.value(HOUR_FORMAT).toInt()));
        setTypeTemperature(static_cast<eTypeTemperature>(config.value(TYPE_TEMPERATURE).toInt()));
        setCity(config.value(CITY).toString());
        setCountry(config.value(COUNTRY).toString());
        setLanguage(static_cast<eLanguage>(config.value(LANGUAGE).toInt()));
        setCommunicationType(static_cast<eTypeCommunicationBME280>(config.value(COMMUNICATION_TYPE).toInt()));
        setUpdateBME280Time(config.value(UPDATE_BME280).toInt());
        setUpdateHTTPTime(config.value(UPDATE_HTTP).toInt());
        setDayStyleSheet(config.value(DAY_STYLE_SHEET).toString());
        setNightStyleSheet(config.value(NIGHT_STYLE_SHEET).toString());
        setDayMode(static_cast<eDayMode>(config.value(DAY_MODE).toInt()));
        FontPreferences fDay;
        fDay.fromString(config.value(DAY_FONT).toString());
        setDayFont(fDay);
        FontPreferences fNight;
        fNight.fromString(config.value(NIGHT_FONT).toString());
        setNightFont(fNight);
        setIpSea(config.value(IP_SEA).toString());
        setPortSea(config.value(PORT_SEA).toInt());
        setAddressHTTP(config.value(ADDRESS_HTTP).toString());
        setAppId(config.value(APP_ID).toString());
    }
}

QString Preferences::getDayStyleSheet() const
{
    return dayStyleSheet;
}

void Preferences::setDayStyleSheet(const QString &value)
{
    dayStyleSheet = value;
}

QString Preferences::getNightStyleSheet() const
{
    return nightStyleSheet;
}

void Preferences::setNightStyleSheet(const QString &value)
{
    nightStyleSheet = value;
}

eDayMode Preferences::getDayMode() const
{
    return dayMode;
}

void Preferences::setDayMode(const eDayMode &value)
{
    dayMode = value;
}

QString Preferences::getIpSea() const
{
    return ipSea;
}

void Preferences::setIpSea(const QString &value)
{
    ipSea = value;
}

int Preferences::getPortSea() const
{
    return portSea;
}

void Preferences::setPortSea(int value)
{
    portSea = value;
}

QString Preferences::getAddressHTTP() const
{
    return addressHTTP;
}

void Preferences::setAddressHTTP(const QString &value)
{
    addressHTTP = value;
}

QString Preferences::getAppId() const
{
    return appId;
}

void Preferences::setAppId(const QString &value)
{
    appId = value;
}
