QT       += core gui
QT       += network
QT       += charts
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    CityForecast/ForecastClouds/forecastclouds.cpp \
    CityForecast/ForecastMain/forecastmain.cpp \
    CityForecast/ForecastSys/forecastsys.cpp \
    CityForecast/cityforecast.cpp \
    CityForecast/cityforecastobject.cpp \
    CityForecast/cityforecastservice.cpp \
    CityForecast/forecastobject.cpp \
    CityForecast/forecastread.cpp \
    CityWeather/Clouds/clouds.cpp \
    CityWeather/Coord/coord.cpp \
    CityWeather/SysCountry/syscountry.cpp \
    CityWeather/TempMain/tempmain.cpp \
    CityWeather/Weather/WeatherObject/weatherobject.cpp \
    CityWeather/Weather/weather.cpp \
    CityWeather/Wind/wind.cpp \
    CityWeather/cityweather.cpp \
    CityWeather/cityweatherservice.cpp \
    CityWeather/readweather.cpp \
    Forms/Forecast/forecastdialog.cpp \
    Forms/Preferences/preferencesdialog.cpp \
    Forms/Styles/styledialog.cpp \
    Preferences/fontpreferences.cpp \
    Preferences/preferences.cpp \
    SeaWeather/seaweather.cpp \
    SeaWeather/seaweatherservice.cpp \
    Utils/CountryInfos/countriesmap.cpp \
    Utils/CountryInfos/countryinfos.cpp \
    Utils/utils.cpp \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    CityForecast/ForecastClouds/forecastclouds.h \
    CityForecast/ForecastMain/forecastmain.h \
    CityForecast/ForecastSys/forecastsys.h \
    CityForecast/cityforecast.h \
    CityForecast/cityforecastobject.h \
    CityForecast/cityforecastservice.h \
    CityForecast/forecastobject.h \
    CityForecast/forecastread.h \
    CityWeather/Clouds/clouds.h \
    CityWeather/Coord/coord.h \
    CityWeather/SysCountry/syscountry.h \
    CityWeather/TempMain/tempmain.h \
    CityWeather/Weather/WeatherObject/weatherobject.h \
    CityWeather/Weather/weather.h \
    CityWeather/Wind/wind.h \
    CityWeather/cityweather.h \
    CityWeather/cityweatherservice.h \
    CityWeather/readweather.h \
    Forms/Forecast/forecastdialog.h \
    Forms/Preferences/preferencesdialog.h \
    Forms/Styles/styledialog.h \
    Preferences/fontpreferences.h \
    Preferences/preferences.h \
    SeaWeather/seaweather.h \
    SeaWeather/seaweatherservice.h \
    Utils/CountryInfos/countriesmap.h \
    Utils/CountryInfos/countryinfos.h \
    Utils/utils.h \
    mainwindow.h

FORMS += \
    Forms/Forecast/forecastdialog.ui \
    Forms/Preferences/preferencesdialog.ui \
    Forms/Styles/styledialog.ui \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    Images/Images.qrc \
    countries_res.qrc \
    language.qrc \
    stylesheets.qrc

TRANSLATIONS = hellotr_fr.ts
