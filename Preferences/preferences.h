#ifndef PREFERENCES_H
#define PREFERENCES_H

#include <QSettings>

#include "fontpreferences.h"

enum eHourFormat
{
    H12,
    H24
};

enum eTypeTemperature
{
    FAHRENHEIT,
    CELSIUS
};

enum eLanguage
{
    EN,
    FR
};

enum eTypeCommunicationBME280
{
    TCPIP,
    HTTP
};

enum eDayMode
{
    DAY,
    NIGHT,
    AUTO
};

class Preferences
{
public:
    Preferences();
    ~Preferences();

    eHourFormat getHourFormat() const;
    void setHourFormat(const eHourFormat &value);

    eTypeTemperature getTypeTemperature() const;
    void setTypeTemperature(const eTypeTemperature &value);

    QString getCity() const;
    void setCity(const QString &value);

    QString getCountry() const;
    void setCountry(const QString &value);

    FontPreferences getDayFont() const;
    void setDayFont(const FontPreferences &value);

    FontPreferences getNightFont() const;
    void setNightFont(const FontPreferences &value);

    eLanguage getLanguage() const;
    void setLanguage(const eLanguage &value);

    eTypeCommunicationBME280 getCommunicationType() const;
    void setCommunicationType(const eTypeCommunicationBME280 &value);

    int getUpdateBME280Time() const;
    void setUpdateBME280Time(int value);

    int getUpdateHTTPTime() const;
    void setUpdateHTTPTime(int value);

    void SaveConfiguration();
    void LoadConfiguration();

    QString getDayStyleSheet() const;
    void setDayStyleSheet(const QString &value);

    QString getNightStyleSheet() const;
    void setNightStyleSheet(const QString &value);

    eDayMode getDayMode() const;
    void setDayMode(const eDayMode &value);

    QString getIpSea() const;
    void setIpSea(const QString &value);

    int getPortSea() const;
    void setPortSea(int value);


    QString getAddressHTTP() const;
    void setAddressHTTP(const QString &value);

    QString getAppId() const;
    void setAppId(const QString &value);

private:
    eHourFormat hourFormat;
    eTypeTemperature typeTemperature;
    QString city;
    QString country;

    FontPreferences dayFont;
    FontPreferences nightFont;

    eLanguage language;

    eTypeCommunicationBME280 communicationType;

    QString dayStyleSheet;
    QString nightStyleSheet;

    QString ipSea;
    int portSea;

    QString addressHTTP;
    QString appId;

    eDayMode dayMode;

    int updateBME280Time;
    int updateHTTPTime;

    const QString CONFIGURATION_FILENAME = "meteo.ini";
    const QString HOUR_FORMAT = "SETTINGS/HOUR";
    const QString TYPE_TEMPERATURE = "SETTINGS/TEMPERATURE_TYPE";
    const QString CITY = "SETTINGS/CITY";
    const QString COUNTRY = "SETTINGS/COUNTRY";
    const QString LANGUAGE = "SETTINGS/LANGUAGE";
    const QString COMMUNICATION_TYPE = "SETTINGS/COMMUNICATION_TYPE";
    const QString UPDATE_BME280 = "SETTINGS/UPDATE_BME280";
    const QString UPDATE_HTTP = "SETTINGS/UPDATE_HTTP";
    const QString DAY_STYLE_SHEET = "STYLESHEET/DAY_STYLE";
    const QString NIGHT_STYLE_SHEET = "STYLESHEET/NIGHT_STYLE";
    const QString DAY_MODE = "SETTINGS/MODE";
    const QString DAY_FONT = "FONT/DAY";
    const QString NIGHT_FONT = "FONT/NIGHT";

    const QString IP_SEA = "SEA/IP";
    const QString PORT_SEA = "SEA/PORT";

    const QString ADDRESS_HTTP = "HTTP/ADDRESS";
    const QString APP_ID = "HTTP/APP_ID";
};

#endif // PREFERENCES_H
