#include "weatherobject.h"

WeatherObject::WeatherObject(int id, QString woMain, QString description, QString icon)
{
    setId(id);
    setWoMain(woMain);
    setDescription(description);
    setIcon(icon);
}

int WeatherObject::getId() const
{
    return id;
}

void WeatherObject::setId(int value)
{
    id = value;
}

QString WeatherObject::getWoMain() const
{
    return woMain;
}

void WeatherObject::setWoMain(const QString &value)
{
    woMain = value;
}

QString WeatherObject::getDescription() const
{
    return description;
}

void WeatherObject::setDescription(const QString &value)
{
    description = value;
}

QString WeatherObject::getIcon() const
{
    return icon;
}

void WeatherObject::setIcon(const QString &value)
{
    icon = value;
}

