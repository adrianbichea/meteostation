#include <QDebug>

#include "cityforecastservice.h"
#include "Utils/utils.h"

CityForecastService::CityForecastService(QString city, QString country, QString address, QString appId, int timeToRead, QString lang)
{
    setCity(city + "," + country);
    setAddress(address);
    setAppId(appId);

    forecastRead = new ForecastRead();
    forecast = forecastRead->Read(getAddress(), getCity(), getAppId(), lang);

    setLang(lang);

    this->timeToRead = timeToRead;
    currentTimeToRead = 0;

    timerForecast = new QTimer();
    timerForecast->setInterval(1000);
    connect(timerForecast, &QTimer::timeout, this, &CityForecastService::onTimerForecast_Tick);
    timerForecast->start();
}

CityForecastService::~CityForecastService()
{
    delete timerForecast;
    delete forecastRead;
}

CityForecast *CityForecastService::getForecast()
{
    return forecast;
}

QString CityForecastService::remainingTimeToRead()
{
    int restTime = (timeToRead - currentTimeToRead);
    if (restTime < 0)
    {
        restTime = 0;
    }
    int minutes = restTime / 60;
    int seconds = restTime - (minutes * 60);
    return Utils::getNumberWithDigits(minutes, 2) + ":" + Utils::getNumberWithDigits(seconds, 2);
}

void CityForecastService::onTimerForecast_Tick()
{
    currentTimeToRead++;
    if (currentTimeToRead >= timeToRead)
    {
        ForceRead();
    }
}

QString CityForecastService::getLang() const
{
    return lang;
}

void CityForecastService::setLang(const QString &value)
{
    lang = value;
}

int CityForecastService::getTimeToRead() const
{
    return timeToRead;
}

void CityForecastService::setTimeToRead(int value)
{
    timeToRead = value;
}

void CityForecastService::ForceRead()
{
    currentTimeToRead = 0;
    auto fc = forecastRead->Read(getAddress(), getCity(), getAppId(), getLang());
    if (forecast != nullptr)
    {
        delete forecast;
    }
    forecast = fc;
}

QString CityForecastService::getAppId() const
{
    return appId;
}

void CityForecastService::setAppId(const QString &value)
{
    appId = value;
}

QString CityForecastService::getAddress() const
{
    return address;
}

void CityForecastService::setAddress(const QString &value)
{
    address = value;
}

QString CityForecastService::getCity() const
{
    return city;
}

void CityForecastService::setCity(const QString &value)
{
    city = value;
}

