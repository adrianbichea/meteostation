#include <QApplication>
#include <QString>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>

#include "forecastread.h"
#include "Utils/utils.h"

ForecastRead::ForecastRead()
{

}

CityForecast* ForecastRead::Read(QString address, QString city, QString appId, QString lang)
{
    QNetworkRequest request(
                        QUrl(address + "/data/2.5/forecast?q=" + city +
                                 "&lang=" + lang +
                                 "&appid=" + appId));
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");


    QNetworkAccessManager nam;

    QNetworkReply *reply = nam.get(request);


    while(!reply->isFinished())
    {
        QApplication::processEvents();
    }

    reply->deleteLater();

    QByteArray response_data = reply->readAll();

    //Conversion du ByteArryay en Json
    QJsonDocument jsonResponse = QJsonDocument::fromJson(response_data);


    QJsonObject jsonObject = jsonResponse.object();

    CityForecast* cf = new CityForecast();

    cf->setCod(jsonObject["cod"].toString());
    cf->setMessage(jsonObject["message"].toInt());
    cf->setCnt(jsonObject["cnt"].toInt());

    QJsonObject objCity = jsonObject["city"].toObject();
    cf->getCity()->setId(objCity["id"].toInt());
    cf->getCity()->setName(objCity["name"].toString());
    cf->getCity()->setCountry(objCity["country"].toString());
    cf->getCity()->setPopulation(objCity["population"].toInt());
    cf->getCity()->setTimezone(objCity["timezone"].toInt());
    cf->getCity()->setSunrise(objCity["sunrise"].toInt());
    cf->getCity()->setSunset(objCity["sunset"].toInt());

    QJsonObject objCityCoord = objCity["coord"].toObject();
    cf->getCity()->getCoord()->setLat(objCityCoord["lat"].toDouble());
    cf->getCity()->getCoord()->setLon(objCityCoord["lon"].toDouble());

    QJsonArray jsonArray = jsonObject["list"].toArray();
    if (jsonArray.size()<=0)
    {
        return nullptr;
    }

    for (int i(0); i < jsonArray.count(); i++)
    {
        ForecastObject* fo = new ForecastObject();
        auto item = jsonArray.at(i);

        fo->setDt(item["dt"].toInt());

        QJsonObject objForecastMain = item["main"].toObject();
        fo->getForecastMain()->setTemp(objForecastMain["temp"].toDouble());
        fo->getForecastMain()->setFeels_like(objForecastMain["feels_like"].toDouble());
        fo->getForecastMain()->setTemp_min(objForecastMain["temp_min"].toDouble());
        fo->getForecastMain()->setTemp_max(objForecastMain["temp_max"].toDouble());
        fo->getForecastMain()->setPressure(objForecastMain["pressure"].toInt());
        fo->getForecastMain()->setSea_level(objForecastMain["sea_level"].toInt());
        fo->getForecastMain()->setGrnd_level(objForecastMain["grnd_level"].toInt());
        fo->getForecastMain()->setHumidity(objForecastMain["humidity"].toInt());
        fo->getForecastMain()->setTemp_kf(objForecastMain["temp_kf"].toDouble());

        QJsonArray objListWeather = item["weather"].toArray();
        foreach(const QJsonValue &weatherValue, objListWeather)
        {
            QJsonObject objWeather = weatherValue.toObject();
            int _id =objWeather["id"].toInt();
            QString _main = objWeather["main"].toString();
            QString _desc =objWeather["description"].toString();
            QString _icon = objWeather["icon"].toString();
            WeatherObject* wo = new WeatherObject(_id, _main, _desc, _icon);
            fo->getWeatherObjects()->push_back(wo);
        }

        QJsonObject objForecastClouds = item["clouds"].toObject();
        fo->getForecastClouds()->setAll(objForecastClouds["all"].toInt());

        QJsonObject objForecastWind = item["wind"].toObject();
        fo->getWind()->setSpeed(objForecastWind["speed"].toDouble());
        fo->getWind()->setDeg(objForecastWind["deg"].toInt());

        fo->setVisibility(item["visibility"].toInt());
        fo->setPop(item["pop"].toDouble());

        QJsonObject objForecastSys = item["sys"].toObject();
        fo->getSys()->setPod(objForecastSys["pod"].toString());

        fo->setDt_txt(item["dt_txt"].toString());

        cf->getForecastObjects()->push_back(fo);
    }
    return cf;
}
