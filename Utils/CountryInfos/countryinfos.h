#ifndef COUNTRYINFOS_H
#define COUNTRYINFOS_H

#include <QString>

class CountryInfos
{
public:
    CountryInfos();
    QString getName() const;
    void setName(const QString &value);

    QString getCountrycode() const;
    void setCountrycode(const QString &value);

    QString getAlpha2() const;
    void setAlpha2(const QString &value);

private:
    QString name;
    QString alpha2;
    QString countrycode;
};

#endif // COUNTRYINFOS_H
