#ifndef CITYFORECASTOBJECT_H
#define CITYFORECASTOBJECT_H

#include <QString>
#include "CityWeather/Coord/coord.h"

class CityForecastObject
{
public:
    CityForecastObject();
    ~CityForecastObject();

    int getId() const;
    void setId(int value);

    Coord *getCoord() const;
    void setCoord(Coord *value);

    QString getCountry() const;
    void setCountry(const QString &value);

    int getPopulation() const;
    void setPopulation(int value);

    int getTimezone() const;
    void setTimezone(int value);

    long getSunrise() const;
    void setSunrise(long value);

    long getSunset() const;
    void setSunset(long value);

    QString getName() const;
    void setName(const QString &value);

private:
    int id;
    QString name;
    Coord* coord;
    QString country;
    int population;
    int timezone;
    long sunrise;
    long sunset;
};

#endif // CITYFORECASTOBJECT_H
