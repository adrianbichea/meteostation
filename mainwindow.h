#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QTranslator>

#include "SeaWeather/seaweatherservice.h"
#include "Preferences/preferences.h"
#include "CityWeather/cityweatherservice.h"
#include "CityForecast/cityforecastservice.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QApplication *myApp, QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void SeaTimer_Tick();

    void on_actionPreferences_triggered();

    void on_actionView_Forecast_triggered();

private:
    Ui::MainWindow *ui;

    QTimer* seaTimer;

    SeaWeatherService* sws;
    CityWeatherService* cws;
    CityForecastService* cfs;

    Preferences p;
    bool bDayNight;

    QApplication *myApplication;
    QTranslator translator;

    void UpdatePreferences();

    void RefreshSeaWeather();
    void RefreshCityWeather();

    void ClearCityWeather();

    void ChangeLanguage(eLanguage lang);
};
#endif // MAINWINDOW_H
