#include "readweather.h"
#include <QApplication>
#include <QString>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include "CityWeather/cityweather.h"

#include <QDebug>
#include <QList>

ReadWeather::ReadWeather()
{
    timerNetwork = new QTimer();
    timerNetwork->setInterval(20000);//20 seconds
    connect(timerNetwork, &QTimer::timeout, this, &ReadWeather::on_TimerNetwork_Tick);
}

ReadWeather::~ReadWeather()
{
    delete timerNetwork;
}

void ReadWeather::on_TimerNetwork_Tick()
{
    timerNetwork->stop();
}

CityWeather* ReadWeather::Read(QString city, QString url, QString appId, QString lang)
{
    QNetworkRequest request(QUrl(url + "/data/2.5/weather?q="
                                 + city + "&lang=" + lang + "&appid=" + appId));
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    QNetworkAccessManager nam;

    QNetworkReply *reply = nam.get(request);

    timerNetwork->start();
    while(!reply->isFinished())
    {
        QApplication::processEvents();
    }
    if (!timerNetwork->isActive())
    {
        qDebug() << "Time out!";
        return nullptr;
    }
    timerNetwork->stop();
    reply->deleteLater();

    QByteArray response_data = reply->readAll();

    //Conversion du ByteArryay en Json
    QJsonDocument jsonResponse = QJsonDocument::fromJson(response_data);


    QJsonObject jsonObject = jsonResponse.object();
    if (jsonObject.isEmpty())
    {
        return nullptr;
    }

    CityWeather* city1 = new CityWeather();

    city1->setId(jsonObject["id"].toInt());
    city1->setTimezone(jsonObject["timezone"].toInt());
    city1->setName(jsonObject["name"].toString());
    QJsonObject rain = jsonObject["rain"].toObject();
    city1->setRain(rain["1h"].toDouble());
    city1->setDt(jsonObject["dt"].toDouble());

    QJsonObject objWind = jsonObject["wind"].toObject();
    city1->getWind()->setSpeed(objWind["speed"].toDouble());
    city1->getWind()->setDeg(objWind["deg"].toInt());

    QJsonObject objMain = jsonObject["main"].toObject();
    city1->getWmain()->setTemp(objMain["temp"].toDouble());
    city1->getWmain()->setFeels_like(objMain["feels_like"].toDouble());
    city1->getWmain()->setTemp_min(objMain["temp_min"].toDouble());
    city1->getWmain()->setTemp_max(objMain["temp_max"].toDouble());
    city1->getWmain()->setPressure(objMain["pressure"].toInt());
    city1->getWmain()->setHumidity(objMain["humidity"].toInt());


    QJsonObject objCoord = jsonObject["coord"].toObject();
    city1->getCoord()->setLat(objCoord["lat"].toDouble());
    city1->getCoord()->setLon(objCoord["lon"].toDouble());


    QJsonObject objSys = jsonObject["sys"].toObject();
    city1->getSys()->setCountryType(objSys["type"].toInt());
    city1->getSys()->setId(objSys["id"].toInt());
    city1->getSys()->setCountry(objSys["country"].toString());
    city1->getSys()->setSunrise(objSys["sunrise"].toInt());
    city1->getSys()->setSunset(objSys["sunset"].toInt());
    city1->getSys()->setCountry(objSys["country"].toString());

    QJsonObject objClouds = jsonObject["clouds"].toObject();
    city1->getClouds()->setAll(objClouds["all"].toInt());

    QJsonArray objListWeather = jsonObject["weather"].toArray();
    foreach(const QJsonValue &weatherValue, objListWeather)
    {
        QJsonObject objWeather = weatherValue.toObject();
        int _id =objWeather["id"].toInt();
        QString _main = objWeather["main"].toString();
        QString _desc =objWeather["description"].toString();
        QString _icon = objWeather["icon"].toString();
        city1->getWeather()->AddWeatherObject(_id,_main,_desc,_icon);
    }
    return city1;
}

bool ReadWeather::ReadyToRead()
{
    return !timerNetwork->isActive();
}
