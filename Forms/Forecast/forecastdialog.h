#ifndef FORECASTDIALOG_H
#define FORECASTDIALOG_H

#include <QDialog>
#include <QList>
#include <QtCharts/QChartView>
#include <QtCharts/QLegend>
#include <QtCharts/QLineSeries>
#include <QtCharts/QCategoryAxis>
#include <QtCharts>


#include <QBrush>


#include "CityForecast/forecastobject.h"

QT_CHARTS_USE_NAMESPACE

namespace Ui {
class ForecastDialog;
}

class ForecastDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ForecastDialog(QWidget *parent = nullptr);
    ~ForecastDialog();

    void ShowChart(QList<ForecastObject*> *forecastObjects, bool celsius);

private:
    Ui::ForecastDialog *ui;

    QChartView *chartViewTemp;
    QChartView *chartViewPerc;
    QChartView *chartViewWind;

    void CreateChartTemp(QList<ForecastObject *> *forecastObjects, bool celsius);
    void CreateChartWind(QList<ForecastObject *> *forecastObjects);
    void CreateChartPerc(QList<ForecastObject *> *forecastObjects);
};

#endif // FORECASTDIALOG_H
