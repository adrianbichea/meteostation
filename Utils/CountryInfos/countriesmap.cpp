#include <QFile>
#include <QDebug>
#include <QString>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

#include "countriesmap.h"
#include "countryinfos.h"

CountriesMap::CountriesMap()
{

}

void CountriesMap::PopulateMap(QString filename)
{
    QFile *file = new QFile(filename);
    if (!file->open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() <<"Fichier de configuration introuvable.";
        return;
    }

    QByteArray response_data = file->readAll();

    qDebug() << "Size: " << response_data.size();

    //Conversion du ByteArryay en Json
    QJsonDocument jsonResponse = QJsonDocument::fromJson(response_data);

    QJsonArray jsonObject = jsonResponse.array();

    foreach(const QJsonValue &countryValue, jsonObject)
    {
        QJsonObject objcountry = countryValue.toObject();
        QString _name =objcountry["name"].toString();
        QString _alpha2 = objcountry["alpha-2"].toString();
        AddToMap(_alpha2,_name);
    }
}

void CountriesMap::AddToMap(QString key, QString value)
{
    map.insert(key, value);
}

QString CountriesMap::getAlpha2(QString countryLongName)
{
    return map.key(countryLongName);
}

QString CountriesMap::getValueFromKey(QString key)
{
    return map[key];
}

QMap<QString, QString> CountriesMap::getMap() const
{
    return map;
}
